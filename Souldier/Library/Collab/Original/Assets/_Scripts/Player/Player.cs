﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class Player : MonoBehaviour
{
    [SerializeField] private float dashForce = 50;
    [SerializeField] private float dashDuration = 0.1f;
    

    public Image stamina, hp, exp;

    public ParticleSystem psr, psn;

    float timeTP = 0;

    public Animator animatorTP;
    public GameState gameState;
    private Rigidbody rb;
    private bool grounded = true;
    private bool dashcd = true;
    private bool cd = false;
    private bool reloading = false;
    private bool preventing = true;
    private bool shieled = false;
    private bool EnableBossDamage = true;
    private bool canTP = false;

    private float distGround;
    Collider PlayerCollider;

    private Scene escena;

    public GameObject DeadPlayer, DeadPlayerTarget;
    
    public GameObject[] Gems;

    bool canBound = false;
    bool bounded = false;
    bool boundDead = false;
    GameObject GOtoBound;
    CapsuleCollider hitbox;
    SphereCollider triggerbox;

    public PlayerStats PS;

    public delegate void UnBound();
    public static event UnBound NoBound;

    public delegate void PlayerDie();
    public static event PlayerDie Die;

    private void OnEnable()
    {
        Abilities.OnShield += Shieled;
        EnemyDrop.Binding += Bound;
        EnemyDrop.Dead += BoundDead;

        EnemySkeleton.Binding += Bound;
        EnemySkeleton.Dead += BoundDead;
    }

    private void OnDisable()
    {
        Abilities.OnShield -= Shieled;
        EnemyDrop.Binding -= Bound;
        EnemyDrop.Dead -= BoundDead;

        EnemySkeleton.Binding -= Bound;
        EnemySkeleton.Dead -= BoundDead;
    }

    private void Awake()
    {
        escena = SceneManager.GetActiveScene();
        rb = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        animatorTP.Play("tpOut");
        PlayerCollider = gameObject.GetComponent<Collider>();
        distGround = PlayerCollider.bounds.extents.y + 0.2f;
        hitbox = gameObject.GetComponent<CapsuleCollider>();
        triggerbox = gameObject.GetComponent<SphereCollider>();
        triggerbox.enabled = false;

        PS.lvl = 0;
    }

    void Update()
    {
        //Funció de Mort del Personatge
        if (hp.fillAmount <= 0.001f)
        {
            Death();
        }

        //Funció que porta la recarga d'energia del personatge.
        ReloadStamina();

        //Posem al ScriptableObject les dades del personatge.
        PS.HP = hp.fillAmount * 100;
        PS.stamina = stamina.fillAmount * 100;
        PS.exp = exp.fillAmount * 100;
        PS.dmgmod = PS.lvl * 0.01f;

        //Pujada de nivell.
        if (exp.fillAmount >= 1)
        {
            PS.lvl++;
            exp.fillAmount = 0;
        }

        //Si el Jugador utilitza l'habilitat de SoulBinding.
        if (bounded)
        {
            //Si l'enemic és un esquelet...
            if (GOtoBound.GetComponent<EnemySkeleton>())
            {
                if (GOtoBound)
                {
                    //El player va a la posició on es troba el cap de l'enemic.
                    transform.position = PS.GOHead.transform.position + new Vector3(0, 0.1f, 0);
                }
            }
            else
            {
                //Si no, simplement es posiciona a sobre.
                transform.position = GOtoBound.transform.position + new Vector3(0, 1, 0);
            }

            //Si la montura mor, comença la funció UnBounding.
            if (boundDead)
            {
                UnBounding();
                boundDead = false;
            }
        }

        if (Input.GetKeyUp("f") && canBound)
        {
            canBound = false;
            bounded = true;
            triggerbox.enabled = true;
            GetComponent<ThirdPersonCharacterControl>().enabled = false;

            if (GOtoBound.GetComponent<EnemySkeleton>())
            {
                transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
            }

            GetComponent<Rigidbody>().useGravity = false;
            if (GOtoBound)
            {
                transform.parent = GOtoBound.transform;
            }
            hitbox.isTrigger = true;
        }

        if (Input.GetKeyDown("f") && !canBound && bounded)
        {
            UnBounding();
        }

        // shield resta stamina
        if (shieled)
        {
            StaminaShield();
        }

        if (Input.GetKeyUp(KeyCode.Mouse1))
        {
            cd = false;
        }

        if (!bounded)
        {
            // funcio de salt
            if (Input.GetKeyDown("space") && (grounded || IsGrounded()) && !shieled)
            {
                grounded = false;
                rb.AddForce(transform.up * 30, ForceMode.Impulse);
            }

            // si premem Shift, inicia la corrutina del dash.
            if (Input.GetKeyDown(KeyCode.LeftShift) && !shieled)
            {
                StartCoroutine(CastDash());
            }

            // basic attack resta stamina.
            if (Input.GetKeyDown(KeyCode.Mouse0) && !shieled)
            {
                if (Gems[0].activeSelf)
                {
                    stamina.fillAmount -= 0.05f;
                }
                else if (Gems[1].activeSelf)
                {
                    stamina.fillAmount -= 0.08f;
                }
                else if (Gems[2].activeSelf)
                {
                    stamina.fillAmount -= 0.1f;
                }
                cd = false;
            }

            // Tepeacion maxima
            if (Input.GetKeyUp(KeyCode.H))
            {
                timeTP = 0;
            }
            if (canTP && Input.GetKey(KeyCode.H))
            {
                if (animatorTP != null)
                {
                    animatorTP.Play("tp");
                }
                timeTP += Time.deltaTime;
                if (timeTP > 3)
                {
                    if (escena.name == "Cementerio")
                    {
                        SceneManager.LoadScene("Forest");
                    }
                    else
                    {
                        SceneManager.LoadScene("Cementerio");
                    }
                }
            }
        } else
        {
            if (GOtoBound.GetComponent<MagicAttack>())
            {
                if (Input.GetKeyDown(KeyCode.Mouse0) && !shieled && stamina.fillAmount > 0)
                {
                    stamina.fillAmount -= 0.05f;
                    cd = false;
                }
            }
        }
    }

    #region Dash
    //Mecanica de Dash del personatge
    public IEnumerator CastDash()
    {
        if (stamina.fillAmount > 0.2 && dashcd)
        {
            //depenent de la direccio en que el personatge estigui moventse, el dash el fara en aquesta direccio.
            // a tots el mateix: aplica una força en la direcció demanada, resta estamina i posa el boolea del dash a false per evitar dashear infinitament.
            if (Input.GetKey("w"))
            {
                rb.AddForce(transform.forward * dashForce, ForceMode.VelocityChange);
                stamina.fillAmount -= 0.2f;
                cd = false;
                dashcd = false;
                StartCoroutine(W8Dash(0.5f));
            }
            else if (Input.GetKey("s"))
            {
                rb.AddForce(transform.forward * -dashForce, ForceMode.VelocityChange);
                stamina.fillAmount -= 0.2f;
                cd = false;
                dashcd = false;
                StartCoroutine(W8Dash(0.5f));
            }
            else if (Input.GetKey("a"))
            {
                rb.AddForce(transform.right * -dashForce, ForceMode.VelocityChange);
                stamina.fillAmount -= 0.2f;
                cd = false;
                dashcd = false;
                StartCoroutine(W8Dash(0.5f));
            }
            else if (Input.GetKey("d"))
            {
                rb.AddForce(transform.right * dashForce, ForceMode.VelocityChange);
                stamina.fillAmount -= 0.2f;
                cd = false;
                dashcd = false;
                StartCoroutine(W8Dash(0.5f));
            }
            //espera un temps
            yield return new WaitForSeconds(dashDuration);
            //para el personatge en sec
            rb.velocity = Vector3.zero;
        } else
        {
            print("reloading dash");
            //particle system de no dash + sonido
            ParticleSystem ps = Instantiate(psn, GameObject.Find("Player").transform.position, Quaternion.identity);
            ps.transform.parent = GameObject.Find("Player").transform;
        }
    }
    #endregion

    public void ReloadStamina()
    {
        // la gracia general d'aquest metode es que un cop feta la habilitat (ja sigui dash o cualsevol altre) no recargui immediatament la estamina
        // si no que tingui un CD avans de començar.
        
        // si alguna habilitat ha sigut triggereada, entra en acció la funcio de recargar stamina.
        if (!cd && preventing)
        {
            preventing = false;
            reloading = false;
            StopCoroutine(CDReload(2));
            StartCoroutine(CDReload(2));
        }
        
        // si la barra no esta plena i es pot recargar, comneça a omplir la barra poc a poc.
        if (stamina.fillAmount < 1 && reloading)
        {
            stamina.fillAmount += 0.1f * Time.deltaTime;
            if (stamina.fillAmount >= 1)
            {
                reloading = false;
            }
        }
    }

    public void Shieled()
    {
        shieled = !shieled;
    }

    private void StaminaShield()
    {
        // resta stamina en el temps
        stamina.fillAmount -= 0.3f * Time.deltaTime;
        // comença la funció per recargar.
        cd = false;
        reloading = false;
    }

    private bool IsGrounded()
    {
        Ray ray = new Ray(transform.position, Vector3.down);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            float dist = Vector3.Distance(hit.point, transform.position);
            // print("Dist" + dist + "     " + "DistGround" + distGround + "       " + hit.transform.gameObject.tag);
            if (hit.transform.gameObject.tag == "Player" || hit.transform.gameObject.tag == "PlayerMove")
            {
                Physics.IgnoreCollision(hit.collider, PlayerCollider);
            } else
            {
                if (dist <= distGround)
                {
                    return true;
                }
            }
        }
        return false;
    }

    public void Bound()
    {
        canBound = true;
    }

    public void BoundDead()
    {
        boundDead = true;
    }

    private void UnBounding()
    {
        transform.localScale = new Vector3(1, 1, 1);

        triggerbox.enabled = false;
        GetComponent<ThirdPersonCharacterControl>().enabled = true;
        GetComponent<Rigidbody>().useGravity = true;
        transform.parent = null;
        bounded = false;
        if (GOtoBound)
        {
            // Treiem aquest enemic d'una llista per poder controlar quants en queden
            gameState.ListaEnemigos.Remove(GOtoBound.gameObject);
            Destroy(GOtoBound.gameObject);
        }
        hitbox.isTrigger = false;
        NoBound?.Invoke();
    }

    private void Death()
    {
        bounded = false;

        if (GOtoBound)
        {
            Destroy(GOtoBound.gameObject);
        }

        Instantiate(DeadPlayer, transform.position, transform.rotation);
        Instantiate(DeadPlayerTarget, transform.position + new Vector3(0, 5, 0), transform.rotation);
        Camera.main.transform.parent = null;
        Die?.Invoke();
        transform.position = new Vector3(0, -50, 0);
        hp.fillAmount = 1;
    }

    // enumerator que controla particules del dash :)
    private IEnumerator W8Dash(float time)
    {
        yield return new WaitForSeconds(time);
        dashcd = true;
        // instanciem les particules de recarga del dash i se les podem com a fill a player per evitar que la particula es mogui del lloc.
        ParticleSystem ps = Instantiate(psr, GameObject.Find("Player").transform.position, Quaternion.identity);
        ps.transform.parent = GameObject.Find("Player").transform;
    }

    private IEnumerator CDReload(float time)
    {
        yield return new WaitForSeconds(time);
        cd = true;
        reloading = true;
        preventing = true;
    }
    
    private IEnumerator EnableBossDMG(float t)
    {
        yield return new WaitForSeconds(t);
        EnableBossDamage = true;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!shieled)
        {
            // si toca amb un enemic, rep mal.
            if (collision.gameObject.CompareTag("Enemy"))
            {
                hp.fillAmount -= 0.2f;
                PS.HP -= 20;
            }

            // si toca amb un atac enemic, rep mal.
            if (collision.gameObject.CompareTag("EnemyFireBall"))
            {
                hp.fillAmount -= 0.4f;
                PS.HP -= 40;
            }

            // en tocar amb el terra, podrà tornar a saltar.
            if (collision.gameObject.CompareTag("Floor"))
            {
                grounded = true;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        // si agafa un orbe de vida, es cura.
        if (other.gameObject.CompareTag("HPOrb"))
        {
            hp.fillAmount += 0.10f;
        }

        if (other.gameObject.CompareTag("water"))
        {
            hp.fillAmount = 0f;
        }

        // si agafa un orbe de stamina, regenera.
        if (other.gameObject.CompareTag("EOrb"))
        {
            stamina.fillAmount += 0.2f;
        }

        // si agafa un orbe de experiencia, augmenta el nivell.
        if (other.gameObject.CompareTag("ExpOrb"))
        {
            if (PS.lvl <= 1)
            {
                exp.fillAmount += 0.05f;
            } else if (PS.lvl > 1 && PS.lvl <= 5)
            {
                exp.fillAmount += 0.01f;
            } else if (PS.lvl > 5)
            {
                exp.fillAmount += 0.005f;
            }
        }

        if (other.tag == "tp")
        {
            canTP = true;
            Debug.Log("Entra collider TP");
        }

        if (!bounded)
        {
            if (other.tag == "CanBound")
            {
                GOtoBound = other.gameObject;
                PS.GOtoBound = other.gameObject;
            }
        }

        if (!shieled)
        {
            if (other.tag == "dmg")
            {
                hp.fillAmount -= 0.02f;
            }
            if (other.tag == "dmgGolem")
            {
                hp.fillAmount -= 0.2f;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "tp")
        {
            canTP = false;
            Debug.Log("Sale collider TP");
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (!shieled)
        {
            // si toca amb un atac enemic, rep mal.
            if (other.gameObject.CompareTag("BossDMG") && EnableBossDamage)
            {
                hp.fillAmount -= 0.33f;
                EnableBossDamage = false;
                StartCoroutine(EnableBossDMG(1.5f));
            }
            // si toca amb un atac del Golem Boss, rep mal.
            if (other.gameObject.CompareTag("BossDMGolem") && EnableBossDamage)
            {
                hp.fillAmount -= 0.4f;
                EnableBossDamage = false;
                StartCoroutine(EnableBossDMG(1.5f));
            }
        }

        if (other.tag == "Chest")
        {
            Debug.Log("Entra Chest");
            if (Input.GetKeyDown("f"))
            {
                other.gameObject.GetComponent<Chest>().DropLoot();
            }
        }
        
        if (!bounded)
        {
            if (other.tag == "CanBound")
            {
                GOtoBound = other.gameObject;
                PS.GOtoBound = other.gameObject;
            }
        }
    }
}
