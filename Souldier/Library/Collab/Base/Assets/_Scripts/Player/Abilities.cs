﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Abilities : MonoBehaviour
{
    public GameObject fireball;
    private Vector3 playerpos;
    private Transform playerotation;

    //PLANING
    //HABILIDAD CLICK DER --> COGER/LANZAR OBJETOS 

    void Update()
    {
        playerpos = GameObject.Find("Player").transform.position;
        playerotation = GameObject.Find("Player").transform;

        //Click esquerre del mouse, atac principal, FireBall.
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Instantiate(fireball, new Vector3(playerpos.x, playerpos.y + 0.5f, playerpos.z), playerotation.rotation);
        }

        if (Input.GetKey(KeyCode.Mouse1))
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                var selection = hit.transform;
                print(selection.gameObject);
                Vector3 direction = Camera.main.transform.position + Input.mousePosition;
                Debug.DrawRay(Camera.main.transform.position, direction, Color.green);
            }
        }
    }
}
