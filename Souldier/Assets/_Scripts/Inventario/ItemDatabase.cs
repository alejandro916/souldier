﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDatabase : MonoBehaviour
{
    public List<Item> items = new List<Item>();

    private void Awake()
    {
        BuildDatabase();
    }

    public Item GetItem(int id)
    {
        return items.Find(item => item.id == id);
    }

    public Item GetItem(string itemName)
    {
        return items.Find(item => item.title == itemName);
    }

    void BuildDatabase()
    {
        items = new List<Item>() {
                new Item(0, "Health potion", "Has healing properties.",
                new Dictionary<string, int>
                {
                    {"Healing", 25 }
                }),
                new Item(1, "Stamina potion", "Restores some stamina.",
                new Dictionary<string, int>
                {
                    {"Stamina recovery", 40 },
                }),
                new Item(2, "Red power gem", "A gem bursting with power.",
                new Dictionary<string, int>
                {
                    {"Power", 10 },
                    {"Stamina usage", 5 }
                }),
                new Item(3, "Mystical magenta gem", "A gem bursting with mystical energy.",
                new Dictionary<string, int>
                {
                    {"Power", 20 },
                    {"Stamina usage", 10 }
                }),
                new Item(4, "Orange power gem", "A gem bursting with flaming energy.",
                new Dictionary<string, int>
                {
                    {"Power", 20 },
                    {"Stamina usage", 10 }
                }),
                new Item(5, "Lime power gem", "A gem bursting with revitalizing energy.",
                new Dictionary<string, int>
                {
                    {"Power", 50 },
                    {"Stamina usage", 10 }
                }),
                new Item(6, "Water gem", "A gem full of useless water.",
                new Dictionary<string, int>
                {
                    {"Power", 25 },
                    {"Stamina usage", 8 }
                })
            };
    }
}
