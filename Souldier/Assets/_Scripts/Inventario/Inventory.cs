﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public List<Item> characterItems = new List<Item>();
    public ItemDatabase itemDatabase;
    public UIInventory inventoryUI;
    public GameObject Hud;
    private Player Player;
    private bool itemCollision;
    private InventoryStatsController statsController;

    public GameObject[] Gems;

    private void Start()
    {
        itemCollision = false;
        Player = GameObject.Find("PlayerMove").GetComponent<Player>();
        inventoryUI.gameObject.SetActive(false);
        statsController = GameObject.Find("StatsController").GetComponent<InventoryStatsController>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            inventoryUI.gameObject.SetActive(!inventoryUI.gameObject.activeSelf);// Si esta activat es desactiva y al reves
            Hud.gameObject.SetActive(!Hud.gameObject.activeSelf);// Si esta activat es desactiva y al reves
            if (Cursor.visible)
            {
                this.GetComponent<ThirdPersonCharacterControl>().enabled = true;
                //this.GetComponent<Player>().enabled = true;
                this.GetComponentInChildren<ThirdPersonCameraControl>().enabled = true;
                this.GetComponentInChildren<rotationTarget>().enabled = true;
                this.GetComponentInChildren<Abilities>().enabled = true;
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
                // Si desactivem l'inventari bloquegem el cursor
            }
            else
            {
                this.GetComponent<ThirdPersonCharacterControl>().enabled = false;
                this.GetComponentInChildren<ThirdPersonCameraControl>().enabled = false;
                //this.GetComponent<Player>().enabled = false;
                this.GetComponentInChildren<rotationTarget>().enabled = false;
                this.GetComponentInChildren<Abilities>().enabled = false;
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
                // Si activem l'inventari desbloquegem el cursor
            }
        }

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            ItemFunction(1);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            ItemFunction(2);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            ItemFunction(3);
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            ItemFunction(4);
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            ItemFunction(5);
        }
    }

    public void ItemFunction(int num)
    {
        if (inventoryUI.UiItems[num-1] != null)// Si hi ha un item
        {
            switch (inventoryUI.UiItems[num - 1].item.id) // Una funció en funció del Id del item
            {
                case 0://HP Potion
                    inventoryUI.UpdateSlot(num - 1, null);
                    Player.hp.fillAmount += 0.25f;
                    break;
                case 1://Stamina Potion
                    inventoryUI.UpdateSlot(num-1, null);
                    Player.stamina.fillAmount += 0.4f;
                    break;
                case 2://Red Gem
                    UpdateGemStats(inventoryUI.UiItems, num);
                    inventoryUI.UpdateSlot(num - 1, null);
                    Gems[0].SetActive(true);
                    Gems[1].SetActive(false);
                    Gems[2].SetActive(false);
                    break;
                case 5://Green Gem
                    UpdateGemStats(inventoryUI.UiItems, num);
                    inventoryUI.UpdateSlot(num - 1, null);
                    Gems[0].SetActive(false);
                    Gems[1].SetActive(false);
                    Gems[2].SetActive(true);
                    break;
                case 6://Blue Gem
                    UpdateGemStats(inventoryUI.UiItems, num);
                    inventoryUI.UpdateSlot(num - 1, null);
                    Gems[0].SetActive(false);
                    Gems[1].SetActive(true);
                    Gems[2].SetActive(false);
                    break;
                    // MAS FUNCIONES?
            }
        }
    }

    private void UpdateGemStats(List<UIItem> UiItems, int num)
    {
        statsController.Gem.sprite = inventoryUI.UiItems[num - 1].item.icon;
        statsController.GemName.text = inventoryUI.UiItems[num - 1].item.title;
        string statText = "\n";
        if (inventoryUI.UiItems[num - 1].item.stats.Count > 0) //Si el item te estadistiques
        {

            foreach (var stat in inventoryUI.UiItems[num - 1].item.stats)
            {
                statText += stat.Value.ToString() + "\n\n";
            }
        }
        statsController.GemStats.text = statText.Trim();

    }

    public void GiveItem(int id)
    {
        // Com donar un item al jugador
        Item itemToAdd = itemDatabase.GetItem(id); // Busquem a la bbdd
        characterItems.Add(itemToAdd); // Afegim a la seva llista
        inventoryUI.AddNewItem(itemToAdd);
    }

    public void GiveItem(string itemName)
    {
        Item itemToAdd = itemDatabase.GetItem(itemName);
        characterItems.Add(itemToAdd);
    }

    public Item CheckForItem(int id)
    {
        return characterItems.Find(item => item.id == id);
        // comprova si el jugador te aquest item
    }

    public void RemoveItem(int id)
    {
        Item item = CheckForItem(id);// Comprovem si te aquest item
        if (item != null)
        {
            characterItems.Remove(item);//Eliminem el item
            inventoryUI.RemoveItem(item);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("RedPoti") && !itemCollision)
        {
            itemCollision = true;
            StartCoroutine(ItemPickUp());
            Destroy(other.transform.parent.gameObject);
            GiveItem(0);
        }
        if (other.gameObject.CompareTag("GreenPoti") && !itemCollision)
        {
            itemCollision = true;
            StartCoroutine(ItemPickUp());
            Destroy(other.transform.parent.gameObject);
            GiveItem(1);
        }
        if (other.gameObject.CompareTag("RedGem") && !itemCollision)
        {
            itemCollision = true;
            StartCoroutine(ItemPickUp());
            Destroy(other.gameObject);
            GiveItem(2);
        }
        if (other.gameObject.CompareTag("BlueGem") && !itemCollision)
        {
            itemCollision = true;
            StartCoroutine(ItemPickUp());
            Destroy(other.gameObject);
            GiveItem(6);
        }
        if (other.gameObject.CompareTag("GreenGem") && !itemCollision)
        {
            itemCollision = true;
            StartCoroutine(ItemPickUp());
            Destroy(other.gameObject);
            GiveItem(5);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("RedPoti") && !itemCollision)
        {
            itemCollision = true;
            StartCoroutine(ItemPickUp());
            Destroy(collision.gameObject);
            GiveItem(0);
        }
        if (collision.gameObject.CompareTag("GreenPoti") && !itemCollision)
        {
            itemCollision = true;
            StartCoroutine(ItemPickUp());
            Destroy(collision.gameObject);
            GiveItem(1);
        }
        if (collision.gameObject.CompareTag("RedGem") && !itemCollision)
        {
            itemCollision = true;
            StartCoroutine(ItemPickUp());
            Destroy(collision.gameObject);
            GiveItem(2);
        }
        if (collision.gameObject.CompareTag("BlueGem") && !itemCollision)
        {
            itemCollision = true;
            StartCoroutine(ItemPickUp());
            Destroy(collision.gameObject);
            GiveItem(6);
        }
        if (collision.gameObject.CompareTag("GreenGem") && !itemCollision)
        {
            itemCollision = true;
            StartCoroutine(ItemPickUp());
            Destroy(collision.gameObject);
            GiveItem(5);
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        itemCollision = false;
    }

    private IEnumerator ItemPickUp()
    {
        yield return new WaitForSeconds(0.001f);
        itemCollision = false;
    }
}
