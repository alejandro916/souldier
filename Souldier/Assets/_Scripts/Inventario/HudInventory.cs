﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HudInventory : MonoBehaviour
{
    public int n; // numero de posició del inventari
    public UIItem UIItem;
    public UIInventory uiInventory;

    // Update is called once per frame
    void Update()
    {
        UIItem[] ArrayUIitems = uiInventory.UiItems.ToArray();
        
        if (ArrayUIitems[n].item == null)
        {
            UIItem.GetComponent<Image>().color = Color.clear; // Transparent
        } else
        {
            UIItem.GetComponent<Image>().sprite = ArrayUIitems[n].item.icon;
            UIItem.GetComponent<Image>().color = Color.white;
        }
    }
}
