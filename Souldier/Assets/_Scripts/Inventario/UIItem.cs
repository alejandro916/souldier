﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIItem : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    // La interfaz IPointerClickHandler nos da acceso a una funcion que se ejecuta cada vez que clickamos en el canvas
    public Item item;
    private Image spriteImage;
    private UIItem selectedItem;
    private Tooltip tooltip;
    private bool itemDebajo;
    private Player player;
    public GameObject[] Gems;
    private InventoryStatsController statsController;

    private void Awake()
    {
        itemDebajo = false;
        spriteImage = GetComponent<Image>();
        UpdateItem(null);
        selectedItem = GameObject.Find("SelectedItem").GetComponent<UIItem>();
        tooltip = GameObject.Find("Tooltip").GetComponent<Tooltip>();
        player = GameObject.Find("PlayerMove").GetComponent<Player>();
        statsController = GameObject.Find("StatsController").GetComponent<InventoryStatsController>();
        Gems = new GameObject[3];
        Gems[0] = GameObject.Find("Red_Gem");
        Gems[1] = GameObject.Find("Blue_Gem");
        Gems[2] = GameObject.Find("Green_Gem");
        Invoke("DisableGemsStart", 0.2f);
    }

    public void DisableGemsStart()
    {
        for (int i = 0; i < Gems.Length; i++)
        {
            if (i != 0)
            {
                Gems[i].gameObject.SetActive(false);
            } else
            {
                Gems[0].gameObject.SetActive(true);
            }
        }
    }

    public void DisableGems(int Ignore)
    {
        for (int i = 0; i < Gems.Length; i++)
        {
            if (i != Ignore)
            {
                Gems[i].gameObject.SetActive(false);
            }
            else
            {
                Gems[i].gameObject.SetActive(true);
            }
        }
    }

    private void Update()
    {
        ItemFunction();
    }

    public void UpdateItem(Item item)
    {
        this.item = item;
        if (this.item != null)
        {
            spriteImage.color = Color.white;
            spriteImage.sprite = this.item.icon;// Accedeix a les dades de la classe item
            this.transform.localScale = new Vector3(0.8f, 0.9f, 1);
        } else
        {
            spriteImage.color = Color.clear;
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (this.item != null) // Comprovem que on s'ha clickat hi hagi un item
        {
            if (selectedItem.item != null) // Si hi habia un item seleccionat fa una copia
            {
                Item itemClon = new Item(selectedItem.item);
                selectedItem.UpdateItem(this.item);// Canvia el item ja seleccionat i la nova selecció
                UpdateItem(itemClon);
            }
            else
            {
                // Si no hi ha item previ seleccionat
                selectedItem.UpdateItem(this.item);
                UpdateItem(null);
            }
        } else if (selectedItem.item != null) // Si a l'inventari no hi ha item
        {
            UpdateItem(selectedItem.item);
            selectedItem.UpdateItem(null);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (this.item != null)
        {
            tooltip.GenerateTooltip(this.item);
            itemDebajo = true;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        tooltip.gameObject.SetActive(false);
        itemDebajo = false;
    }

    public void ItemFunction()
    {
        if (Input.GetKeyDown("u") && itemDebajo)
        {
            tooltip.gameObject.SetActive(false);
            switch (this.item.id)
            {
                case 0:
                    player.hp.fillAmount += 0.2f;
                    break;
                case 1:
                    player.stamina.fillAmount += 0.2f;
                    break;
                case 2:
                    UpdateGemStats(this.item);
                    DisableGems(0);
                    break;
                case 5:
                    UpdateGemStats(this.item);
                    DisableGems(2);
                    break;
                case 6:
                    UpdateGemStats(this.item);
                    DisableGems(1);
                    break;
            }
            this.item = null;
            UpdateItem(this.item);
        }
    }

    private void UpdateGemStats(Item item)
    {
        statsController.Gem.sprite = item.icon;
        statsController.GemName.text = item.title;
        string statText = "\n";
        if (item.stats.Count > 0) //Si el item te estadistiques
        {

            foreach (var stat in item.stats)
            {
                statText += stat.Value.ToString() + "\n\n";
            }
        }
        statsController.GemStats.text = statText.Trim();
    }

}
