﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryStatsController : MonoBehaviour
{
    private GameObject Player;
    public Image HP, Gem;
    public Text GemName, GemStats;
    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.Find("PlayerMove");
    }

    // Update is called once per frame
    void Update()
    {
        HP.fillAmount = Player.GetComponent<Player>().hp.fillAmount;
    }
}
