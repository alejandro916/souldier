﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Tooltip : MonoBehaviour
{
    private Text tooltipText;

    // Start is called before the first frame update
    void Start()
    {
        //per defecte volem el tooltip ocult
        tooltipText = GetComponentInChildren<Text>();
        tooltipText.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GenerateTooltip(Item item)
    {
        string statText = "";
        if (item.stats.Count > 0) //Si el item te estadistiques
        {
            foreach(var stat in item.stats)
            {
                statText += stat.Key.ToString() + ": " + stat.Value.ToString() + "\n";
            }
        }
        string tooltip = string.Format("<b>{0}</b>\n{1}\n\n<b>{2}</b>", 
            item.title, item.description, statText);
        tooltipText.text = tooltip;
        tooltipText.gameObject.SetActive(true);
        gameObject.SetActive(true);
    }
}
