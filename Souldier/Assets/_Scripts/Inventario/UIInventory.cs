﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIInventory : MonoBehaviour
{
    public List<UIItem> UiItems = new List<UIItem>();
    public GameObject slotPrefab;
    public Transform slotPanel;
    public int numberOfSlots;

    private void Awake()
    {
        for (int i = 0; i < numberOfSlots; i++)
        {
            // Genera una ranura a l'inventari fins al limit proposat a la variable publica
            GameObject instance = Instantiate(slotPrefab);
            instance.transform.SetParent(slotPanel);
            instance.transform.localScale = new Vector3(1, 1, 1);
            UiItems.Add(instance.GetComponentInChildren<UIItem>());
        }
    }

    public void UpdateSlot(int slot, Item item)
    {
        UiItems[slot].UpdateItem(item);
    }

    public void AddNewItem(Item item)
    {
        // Busca el primer espai buit i si hi ha inserta l'item
        UpdateSlot(UiItems.FindIndex(i => i.item == null), item);
    }

    public void RemoveItem(Item item)
    {
        // Busca l'item i l'elimina
        UpdateSlot(UiItems.FindIndex(i => i.item == item), null);
    }
}

