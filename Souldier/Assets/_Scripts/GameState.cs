﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState : MonoBehaviour
{
    public Animator piedras;
    public Animator puerta;

    public GameObject CamPuerta;
    public GameObject CamPiedras;
    public GameObject camPlayer;
    public GameObject puertaSonido;

    public GameObject Golem;
    private bool done = false;
    private bool noEnemies = false;
    public List<GameObject> ListaEnemigos;

    void Start()
    {
        
    }

    void Update()
    {
        if (Golem == null && !done)
        {
            StartCoroutine(CinematicaPuerta());
            puertaSonido.GetComponent<AudioSource>().enabled = true;
            puerta.SetBool("openDoor", true);
            done = true;
        }
        if (ListaEnemigos.Count == 0 && !noEnemies)
        {
            StartCoroutine(CinematicaPiedras());
            piedras.SetBool("BajarPiedras", true);
            noEnemies = true;
        }
        //Debug.Log(ListaEnemigos.Count);
    }

    private IEnumerator CinematicaPuerta()
    {
        if (camPlayer == null)
        {
            CamPuerta.SetActive(true);
            yield return new WaitForSeconds(7);
            camPlayer.SetActive(true);
            CamPuerta.SetActive(false);
        }
        else
        {
            camPlayer.SetActive(false);
            CamPuerta.SetActive(true);
            yield return new WaitForSeconds(7);
            camPlayer.SetActive(true);
            CamPuerta.SetActive(false);
        }
    }

    private IEnumerator CinematicaPiedras()
    {
        if (camPlayer == null)
        {
            CamPiedras.SetActive(true);
            yield return new WaitForSeconds(4);
            camPlayer.SetActive(true);
            CamPiedras.SetActive(false);
        }
        else
        {
            camPlayer.SetActive(false);
            CamPiedras.SetActive(true);
            yield return new WaitForSeconds(4);
            camPlayer.SetActive(true);
            CamPiedras.SetActive(false);
        }
    }
}
