﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class Pause : MonoBehaviour
{
    public static bool isPaused = false;

    public GameObject PauseMenu, SettingsMenu, inventory, hud;
    private GameObject player;
    

    private void Start()
    {
        player = GameObject.Find("PlayerMove");
        Continuar();
    }
    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isPaused)
            {
                Continuar();
            }
            else
            {
                Pausa();
            }
        }
        if (isPaused)
        {
            inventory.gameObject.SetActive(false);
            Pausa();
        }
    }

    public void Continuar()
    {
        hud.gameObject.SetActive(true);
        player.GetComponent<ThirdPersonCharacterControl>().enabled = true;
        player.GetComponentInChildren<ThirdPersonCameraControl>().enabled = true;
        player.GetComponentInChildren<rotationTarget>().enabled = true;
        player.GetComponentInChildren<Abilities>().enabled = true;
        player.GetComponent<Player>().enabled = true;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        PauseMenu.SetActive(false);
        Time.timeScale = 1; // Descongela el temps
        isPaused = false;
    }

    void Pausa()
    {
        hud.gameObject.SetActive(false);
        inventory.gameObject.SetActive(false);
        player.GetComponent<ThirdPersonCharacterControl>().enabled = false;
        if (player.GetComponentInChildren<ThirdPersonCameraControl>())
        {
            player.GetComponentInChildren<ThirdPersonCameraControl>().enabled = false;
        }
        player.GetComponentInChildren<rotationTarget>().enabled = false;
        player.GetComponentInChildren<Abilities>().enabled = false;
        player.GetComponent<Player>().enabled = false;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        if (!SettingsMenu.gameObject.activeSelf) {
            PauseMenu.SetActive(true);
        }
        Time.timeScale = 0; // Para el temps
        isPaused = true;
    }

    public void Menu()
    {
        Time.timeScale = 1;
        Continuar();
        SceneManager.LoadScene("MainMenu");
    }

    public void Ajustes()
    {
        PauseMenu.SetActive(false);
        SettingsMenu.SetActive(true);
    }

    public void CerrarApp()
    {
        Application.Quit();
    }
}
