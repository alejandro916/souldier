﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBall : MonoBehaviour
{
    private Rigidbody rb;

    public ParticleSystem explosion;

    public float radius = 5.0f;
    public float power = 100.0f;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        //Comença la corrutina de destrucció de la bola per evitar que s'acumulin si hi ha algun problema.
        StartCoroutine(Efe());
        //Apliquem una força a la bola per a que es llanci cap a endevant.
        rb.AddForce(transform.forward * 20, ForceMode.Impulse);
    }

    private void OnCollisionEnter(Collision collision)
    {
        //Si collisiona amb qualsevol cosa excepte el player, es destrueix i instancia particules d'explosio.
        if (collision.gameObject.CompareTag("Player")) { }
        else
        {
            //Instanciar explosion
            ParticleSystem ps = Instantiate(explosion, transform.position, Quaternion.identity);
            Explosion();
            Destroy(this.gameObject);
        }
    }

    private void Explosion()
    {
        Vector3 explosionPos = transform.position;
        Collider[] colliders = Physics.OverlapSphere(explosionPos, radius);
        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();

            if (rb != null)
                rb.AddExplosionForce(power, explosionPos, radius, 3.0f);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Shield"))
        {
            //Instanciar explosion
            ParticleSystem ps = Instantiate(explosion, transform.position, Quaternion.identity);
            Destroy(this.gameObject);
        }
    }

    private IEnumerator Efe()
    {
        //Despres de 5 segons, es destrueix.
        yield return new WaitForSeconds(5);
        Destroy(this.gameObject);
    }
}
