﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonCameraControl : MonoBehaviour
{
    float rotationSpeed = 1;
    public Transform Target, PlayerTarget;
    float mouseX, mouseY;

    bool dead = false;

    public Transform Obstruction;
    //float zoomSpeed = 2f;

    private void OnEnable()
    {
        Player.Die += Dead;
    }

    private void OnDisable()
    {
        Player.Die -= Dead;
    }

    void Start()
    {
        Obstruction = Target;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void LateUpdate()
    {
        if (dead)
        {
            PlayerTarget = GameObject.FindWithTag("PlayerDead").transform;
            Target = GameObject.FindWithTag("PlayerDeadTarget").transform;
            transform.parent = Target.transform;
        }
        else
        {
            CamControl();
        }
    }

    void CamControl()
    {
        mouseX += Input.GetAxis("Mouse X") * rotationSpeed;
        mouseY -= Input.GetAxis("Mouse Y") * rotationSpeed;
        mouseY = Mathf.Clamp(mouseY, -35, 60);

        transform.LookAt(Target);

        if (Input.GetKey(KeyCode.LeftShift))
        {
            Target.rotation = Quaternion.Euler(mouseY, mouseX, 0);
        }
        else
        {
            Target.rotation = Quaternion.Euler(mouseY, mouseX, 0);
            PlayerTarget.rotation = Quaternion.Euler(0, mouseX, 0);
        }
    }

    public void Dead()
    {
        dead = true;
    }
}