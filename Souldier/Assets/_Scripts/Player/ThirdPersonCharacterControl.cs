﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonCharacterControl : MonoBehaviour
{
    public float Speed = 15;
    private bool shieled = false;

    private void OnEnable()
    {
        Abilities.OnShield += Shieled;
    }

    private void OnDisable()
    {
        Abilities.OnShield -= Shieled;
    }

    void Update ()
    {
        if (shieled)
        {
            PlayerMovement(1);
        }
        else
        {
            if (Input.GetKeyDown("s"))
            {
                Speed /= 2;
            } else if (Input.GetKeyUp("s") || Input.GetKeyDown("e") || Input.GetKeyDown(KeyCode.Escape))
            {
                Speed = 10;
            } else
            {
                Speed = 10;
            }
            PlayerMovement(Speed);
        }
    }

    void PlayerMovement(float speed)
    {
        float hor = Input.GetAxis("Horizontal");
        float ver = Input.GetAxis("Vertical");
        Vector3 playerMovement = new Vector3(hor, 0f, ver) * speed * Time.deltaTime;
        transform.Translate(playerMovement, Space.Self);
    }

    public void Shieled()
    {
        shieled = !shieled;
    }
}