﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Abilities : MonoBehaviour
{
    public GameObject[] fireballs;
    public Image stamina;
    private Vector3 playerpos;
    private Transform playerotation;
    public Transform targetHabilidad;
    public GameObject shield;
    float MaxRange = 12f;
    
    public ParticleSystem psn;
    Transform selection = null;
    Transform prevSelection = null;
    bool canTake = false;
    bool kinesis = false;
    bool pulled = true;
    
    bool shieled = false;
    GameObject shieldGO;

    public GameObject[] Gems;

    bool bounded = false;

    public AudioSource AudioSource;
    public AudioClip FireBallAudio;

    public Camera PlayerCam;

    public PlayerStats PS;

    bool dead = false;

    public delegate void ShieldAction();
    public static event ShieldAction OnShield;

    public delegate void SoulBounding();
    public static event SoulBounding Bounding;

    private void OnEnable()
    {
        EnemyDrop.Binding += Bound;
        EnemySkeleton.Binding += Bound;
        Player.NoBound += UnBound;
        Player.Die += Dead;
    }

    private void OnDisable()
    {
        EnemyDrop.Binding -= Bound;
        EnemySkeleton.Binding -= Bound;
        Player.NoBound -= UnBound;
        Player.Die -= Dead;
    }
    
    void Update()
    {
        //Localitzem el GameObject FireBallSpawn que es on apareixeran els projectils del personatge.
        playerpos = GameObject.Find("FireBallSpawn").transform.position;
        playerotation = GameObject.Find("FireBallSpawn").transform;
        //Localitzem el vector direccional de la camera.
        var CameraDir = (targetHabilidad.transform.position - PlayerCam.transform.position);

        if (!dead)
        {
            #region Basic
            if (!bounded)
            {
                //Click esquerre del mouse, atac principal, FireBall.
                if (Input.GetKeyDown(KeyCode.Mouse0) && stamina.fillAmount > 0 && !kinesis && !shieled)
                {
                    if (Gems[0].activeSelf)
                    {
                        //print("fire 0");
                        Instantiate(fireballs[0], new Vector3(playerpos.x, playerpos.y, playerpos.z), playerotation.rotation);
                        AudioSource.PlayOneShot(FireBallAudio, 1);
                    }
                    else if (Gems[1].activeSelf)
                    {
                        //print("fire 1");
                        Instantiate(fireballs[1], new Vector3(playerpos.x, playerpos.y, playerpos.z), playerotation.rotation);
                        AudioSource.PlayOneShot(FireBallAudio, 1);
                    }
                    else if (Gems[2].activeSelf)
                    {
                        //print("fire 2");
                        Instantiate(fireballs[2], new Vector3(playerpos.x, playerpos.y, playerpos.z), playerotation.rotation);
                        AudioSource.PlayOneShot(FireBallAudio, 1);
                    }
                }
            }
            else
            {
                if (PS.GOtoBound)
                {
                    if (PS.GOtoBound.GetComponent<MagicAttack>())
                    {
                        if (Input.GetKeyDown(KeyCode.Mouse0) && stamina.fillAmount > 0 && !kinesis && !shieled)
                        {
                            Instantiate(fireballs[3], new Vector3(playerpos.x, playerpos.y, playerpos.z), playerotation.rotation);
                            AudioSource.PlayOneShot(FireBallAudio, 1);
                        }
                    }
                }
            }
            #endregion

            #region Shield
            //Premem q, escut  protector de projectils.
            if (Input.GetKeyDown("q") && stamina.fillAmount > 0)
            {
                shieled = true;
                shieldGO = Instantiate(shield, new Vector3(playerpos.x, playerpos.y - 0.6f, playerpos.z), playerotation.rotation);
                shieldGO.transform.parent = GameObject.Find("Player").transform;
                //Invoke a l'event.
                OnShield?.Invoke();
            }

            //Si deixem de pulsar o la stamina arriba a 0, l'escut desapareix.
            if (Input.GetKeyUp("q") && !shieled)
            { }
            else
            if (Input.GetKeyUp("q") || stamina.fillAmount <= 0 && shieled)
            {
                if (shieldGO != null)
                {
                    Destroy(shieldGO.gameObject);
                }
                shieled = false;
                OnShield?.Invoke();
            }
            #endregion

            #region Telek
            //variable ray que apunta de la camera a la posicio del mouse.
            var ray = PlayerCam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            //"simple" raycast per seleccionar un objecte devant del player.
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.gameObject.tag != "Enemy" || hit.transform.gameObject.tag != "CanBound")
                {
                    //variable Transform per guardar el objecte seleccionat
                    selection = hit.transform;
                    //variable secundaria per poder mantenir el objecte anteriorment seleccionat
                    if (prevSelection == null)
                    {
                        prevSelection = selection;
                    }
                    //float que dista el objecte del jugador
                    float dist = Vector3.Distance(selection.transform.position, transform.position);
                    //si la distancia supera el rang maxim previament seleccionat, no podrá agafar l'objecte
                    if (dist <= MaxRange)
                    {
                        //si son el mateix objecte
                        if (selection == prevSelection)
                        {
                            //i el objecte a seleccionar té MeshRenderer
                            if (selection.gameObject.GetComponent<MeshRenderer>() != null)
                            {
                                //augmenta el valor _Outline del shadder proporcionat per fer visible un highlight en l'objecte
                                selection.gameObject.GetComponent<MeshRenderer>().material.SetFloat("_Outline", 1f);
                                //i habilita el boolean per poder agafar l'objecte.
                                canTake = true;
                            }
                        }
                        else
                        {
                            //d'altra banda,
                            if (prevSelection.gameObject.GetComponent<MeshRenderer>() != null)
                            {
                                //elimina l'outline si es que n'hi ha
                                prevSelection.gameObject.GetComponent<MeshRenderer>().material.SetFloat("_Outline", 0f);
                                if (prevSelection.gameObject.GetComponent<Rigidbody>() != null)
                                {
                                    //torna a habilitar la gravetat i les rotacions de l'objecte.
                                    prevSelection.gameObject.GetComponent<Rigidbody>().useGravity = true;
                                    //prevSelection.GetComponent<Rigidbody>().freezeRotation = false;
                                }
                                //posa a null la selecció previa per poder tornarla a igualar.
                                prevSelection = null;
                                canTake = false;
                                kinesis = false;
                            }
                        }
                    }
                    else
                    {
                        //si no entra al rang, té MeshRenderer i Rigidbody.
                        if (prevSelection.gameObject.GetComponent<MeshRenderer>() != null && prevSelection.gameObject.GetComponent<Rigidbody>() != null)
                        {
                            //elimina l'outline si es que n'hi ha
                            prevSelection.gameObject.GetComponent<MeshRenderer>().material.SetFloat("_Outline", 0f);
                            //torna a habilitar la gravetat i les rotacions de l'objecte.
                            prevSelection.gameObject.GetComponent<Rigidbody>().useGravity = true;
                            //prevSelection.GetComponent<Rigidbody>().freezeRotation = false;
                            //posa a null la selecció previa per poder tornarla a igualar.
                            kinesis = false;
                            prevSelection = null;
                            canTake = false;
                        }
                    }
                }
            }

            //si l'objecte es pot agafar i té rigidbody
            if (selection != null && canTake && selection.gameObject.GetComponent<Rigidbody>() != null)
            {
                //en pulsar el botó dret del mouse
                if (Input.GetKey(KeyCode.Mouse1))
                {
                    //habilitem kinesis
                    kinesis = true;
                    //treiem la gravetat a l'objecte seleccionat
                    selection.gameObject.GetComponent<Rigidbody>().useGravity = false;
                    //fem que miri cap a endevant.
                    selection.transform.forward = transform.forward;
                    //si esta sent atret, continuamente anirà cap a la posició d'un objete fixat a la camera.
                    if (pulled)
                    {
                        selection.transform.position = Vector3.Lerp(selection.transform.position, targetHabilidad.transform.position, Time.deltaTime * 40);
                    }
                    //si en el moment en que el tenim agafat, pulsem el mouse equerre i tenim suficient stamina aplica una força, sense tenir en conte la massa del objecte, i el lleça.
                    if (Input.GetKeyDown(KeyCode.Mouse0) && stamina.fillAmount > 0)
                    {
                        pulled = false;
                        selection.GetComponent<Rigidbody>().AddForce(selection.transform.forward * 50, ForceMode.VelocityChange);
                        selection.gameObject.tag = "Projectile";
                    }
                    else if (Input.GetKeyDown(KeyCode.Mouse0) && stamina.fillAmount <= 0)
                    {
                        //si no tenim stamina, apareixen unes particues que ens avisa de que no es pot fer l'acció.
                        ParticleSystem ps = Instantiate(psn, GameObject.Find("Player").transform.position, Quaternion.identity);
                        ps.transform.parent = GameObject.Find("Player").transform;
                    }
                }
                //si deixem de pulsar el botó dret, l'objecte cau.
                if (Input.GetKeyUp(KeyCode.Mouse1))
                {
                    kinesis = false;
                    selection.gameObject.GetComponent<Rigidbody>().useGravity = true;
                    //selection.GetComponent<Rigidbody>().freezeRotation = false;
                }
            }
            else
            {
                //un reset per evitar possibles errors.
                if (Input.GetKey(KeyCode.Mouse1))
                {
                    pulled = true;
                    selection = null;
                    prevSelection = null;
                }
            }
            #endregion

            #region SoulBinding
            if (Input.GetKeyDown("f") && !bounded)
            {
                Bounding?.Invoke();
            }
            #endregion
        }
    }

    public void Bound()
    {
        bounded = true;
    }

    public void UnBound()
    {
        bounded = false;
    }

    public void Dead()
    {
        dead = true;
    }
}
