﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotationTarget : MonoBehaviour
{
    public Camera cam;

    void Update()
    {
        var angle = cam.transform.forward.y;
        //el jugador segueix el forward de la camera
        transform.forward = cam.transform.forward;
        //pero si l'angle es menor a 0, el jugador només mirarà cap a endevant, no cap a abaix.
        if (angle < 0)
        {
            angle += Input.GetAxis("Vertical") * Time.deltaTime * 10;
            angle = Mathf.Clamp(angle, 0, 80);
            transform.localRotation = Quaternion.AngleAxis(angle, Vector3.right);
        }
    }
}
