﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasLookAt : MonoBehaviour
{
    private GameObject player;

    //Script molt basic per a que els canavs dels enemics sempre mirin al jugador.
    void Update()
    {
        player = GameObject.Find("Main Camera");
        if (player)
        {
            transform.LookAt(player.transform);
        }
    }
}
