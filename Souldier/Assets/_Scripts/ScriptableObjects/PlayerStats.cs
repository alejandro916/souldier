﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu]
public class PlayerStats : ScriptableObject
{
    public float HP, stamina, exp, dmgmod;
    public int lvl;
    public GameObject GOtoBound, GOHead;
}
