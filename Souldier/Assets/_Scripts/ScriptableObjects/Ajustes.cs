﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Ajustes : ScriptableObject
{
    [SerializeField]
    public float MusicVolume, ProjectileVolume, AmbientalVolume;

}
