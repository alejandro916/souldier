﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Delete : MonoBehaviour
{
    //Script molt simple per destruir qualsevol cosa x segons despres de la seva instanciacio.

    private void Start()
    {
        //temps random en el que triga a desapareixer.
        float time = Random.Range(2, 4);
        StartCoroutine(Efe(time));
    }

    private IEnumerator Efe(float time)
    {
        //espera temps i es destrueix.
        yield return new WaitForSeconds(time);
        Destroy(this.gameObject);
    }
}
