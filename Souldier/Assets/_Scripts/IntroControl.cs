﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IntroControl : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Invoke("Continue", 7);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Continue()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
