﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Platform : MonoBehaviour
{
    GameObject player;
    float timeTP = 0;
    private bool canTP = false;
    private Scene escena = SceneManager.GetActiveScene();

    void Start()
    {
        player = PlayerManager.instance.player;
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.H))
        {
            timeTP = 0;
        }
        if (canTP && Input.GetKey(KeyCode.H))
        {
            timeTP += Time.deltaTime;
            if (timeTP > 3)
            {
                if (escena.name == "Cementerio")
                {
                    SceneManager.LoadScene("Forest");
                }
                else
                {
                    SceneManager.LoadScene("Cementerio");
                }
            }
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && canTP)
        {
            
        }
    }
}
