﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cinematica : MonoBehaviour
{
    public GameObject Cam1;
    public GameObject Cam2;
    public GameObject cam3;
    public GameObject camPlayer;

    void Start()
    {
        StartCoroutine(Secuencia());
    }

    private IEnumerator Secuencia()
    {
        if (camPlayer == null)
        {
            Cam1.SetActive(true);
            yield return new WaitForSeconds(4);
            Cam2.SetActive(true);
            Cam1.SetActive(false);
            yield return new WaitForSeconds(5);
            cam3.SetActive(true);
            Cam2.SetActive(false);
            yield return new WaitForSeconds(4);
            camPlayer.SetActive(true);
            cam3.SetActive(false);
        }
        else
        {
            camPlayer.SetActive(false);
            Cam1.SetActive(true);
            yield return new WaitForSeconds(4);
            Cam2.SetActive(true);
            Cam1.SetActive(false);
            yield return new WaitForSeconds(5);
            cam3.SetActive(true);
            Cam2.SetActive(false);
            yield return new WaitForSeconds(4);
            camPlayer.SetActive(true);
            cam3.SetActive(false);
        }
    }
}
