﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController1 : MonoBehaviour
{
    public Animator animator;

    public AudioSource Source, Musica;
    public AudioClip Seleccion;
    // Start is called before the first frame update
    void Start()
    {
        Destroy(GameObject.Find("PlayerMove"));
        Destroy(GameObject.Find("Canvas1"));
        Destroy(GameObject.Find("Inventario"));
        Destroy(GameObject.Find("GameOver"));
        DontDestroyOnLoad(Musica); // Conseguim que la musica no es talli entre escenes
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKey)
        {
            Source.PlayOneShot(Seleccion, 0.25f);
            StartCoroutine(Cambio("Cambio", 1));
        }
    }

    IEnumerator Cambio(string animacion, float time)
    {
        animator.PlayInFixedTime(animacion, 0, 1);
        yield return new WaitForSeconds(time);
        SceneManager.LoadScene("MainMenu2");
    }
}
