﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LvlController : MonoBehaviour
{
    public PlayerStats PS;
    public Text lvl;

    void Update()
    {
        lvl.text = PS.lvl + "";
    }
}
