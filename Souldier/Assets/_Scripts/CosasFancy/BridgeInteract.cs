﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BridgeInteract : MonoBehaviour
{

    public Animator animatorPuente;
    public AudioSource Audio;
    public AudioClip AudioClip;

    private void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Projectile") || collision.gameObject.CompareTag("FireBall"))
        {
            animatorPuente.SetBool("cae", true);
            Audio.gameObject.GetComponent<AudioSource>().clip = AudioClip;
            Audio.PlayDelayed(0.5f);
            Destroy(collision.gameObject);
            Destroy(this.gameObject);
        }
    }
}
