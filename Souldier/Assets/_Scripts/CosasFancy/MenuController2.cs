﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController2 : MonoBehaviour
{
    int seleccion = 1;
    public Image[] images;

    public AudioClip sonido;
    public AudioSource Source;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))
        {
            if (seleccion != 1) //per a evitar que la selecció sigui negativa i fora dels valors que volem
            {
                Source.PlayOneShot(sonido, 0.5f);
                seleccion--;
            }
        }
        if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
        {
            if (seleccion != 2) //per a evitar que la selecció sigui negativa i fora dels valors que volem
            {
                Source.PlayOneShot(sonido, 0.5f);
                seleccion++;
            }
        }
        images[seleccion-1].gameObject.SetActive(true); // Activa el valor seleccionat
        switch (seleccion) {
            case 1:
                images[1].gameObject.SetActive(false); // desactiva els valors no seleccionats
                images[2].gameObject.SetActive(false);
                if (Input.GetKeyDown(KeyCode.Return)) //KeyCode.Return == Enter
                {
                    //TODO partida nueva con selector de nombre, el nombre sera el nombre del archivo de save
                    SceneManager.LoadScene("Forest");
                    Source.PlayOneShot(sonido, 0.5f);
                    
                    Destroy(GameObject.Find("Musica"));
                }
                break;
            /*case 2:
                images[0].gameObject.SetActive(false);
                images[2].gameObject.SetActive(false);
                if (Input.GetKeyDown(KeyCode.Return))
                {
                    //TODO cargar partida con selector
                    sonido.Play();
                }
                break;*/
            case 2:
                images[0].gameObject.SetActive(false);
                images[1].gameObject.SetActive(true);
                if (Input.GetKeyDown(KeyCode.Return))
                {
                    Application.Quit(); // Para la execució de l'aplicació, només funciona a la versió build
                }
                break;
        }

    }
}
