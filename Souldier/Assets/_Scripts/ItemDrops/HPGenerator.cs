﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HPGenerator : MonoBehaviour
{
    public GameObject hp;

    private void Start()
    {
        StartCoroutine(W8());
    }

    public IEnumerator W8()
    {
        yield return new WaitForSeconds(2);
        int j = 0;
        int n = Random.Range(1, 3);
        while (j != n)
        {
            float x, y, z;
            x = Random.Range(0, 0.5f);
            y = Random.Range(0, 0.5f);
            z = Random.Range(0, 0.5f);
            Instantiate(hp, new Vector3(transform.position.x + x, transform.position.y + y, transform.position.z + z), Quaternion.identity);
            j++;
        }
        StartCoroutine(W8());
    }
}
