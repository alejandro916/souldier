﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPull : MonoBehaviour
{
    GameObject Player;
    public float forceFactor = 1;

    private void Start()
    {
        //Comença la corrutina de destrucció del item per evitar que s'acumulin si hi ha algun problema.
        StartCoroutine(Efe());
    }

    void Update()
    {
        //Busquem al jugador
        Player = GameObject.Find("Player");
        //Calculem la distancia entre jugador i item.
        float dist = Vector3.Distance(Player.transform.position, transform.position);

        //Si la distancia es troba entre 3 i 6, aplica una força constant al item cap al jugador.
        //Si es menor a 3 unitats, la força es major.
        if (dist <= 6 && dist >= 3)
        {
            GetComponent<Rigidbody>().AddForce((Player.transform.position - transform.position) * forceFactor);
        } else if (dist < 3)
        {
            GetComponent<Rigidbody>().AddForce((Player.transform.position - transform.position) * forceFactor * 3);
        }
    }

    //Si toca amb el jugador, desapareix (Collider)
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("PlayerMove"))
        {
            Destroy(gameObject);
        }
    }

    //Si toca amb el jugador, desapareix (Trigger)
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PlayerMove"))
        {
            Destroy(gameObject);
        }
    }

    private IEnumerator Efe()
    {
        //Despres de 5 segons, es destrueix.
        yield return new WaitForSeconds(20);
        Destroy(this.gameObject);
    }
}
