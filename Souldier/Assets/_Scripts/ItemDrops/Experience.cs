﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Experience : MonoBehaviour
{
    GameObject Player;
    public float forceFactor = 2;

    private void Start()
    {
        //Comença la corrutina de destrucció del item per evitar que s'acumulin si hi ha algun problema.
        StartCoroutine(Efe());
    }

    void Update()
    {
        //Busquem al jugador
        Player = GameObject.Find("Player");
        //Calculem la distancia entre jugador i item.
        /*float dist = Vector3.Distance(Player.transform.position, transform.position);

        //Si la distancia es troba entre 5 i 10, aplica una força constant al item cap al jugador i cap amunt.
        //Si es menor a 5 unitats, la força es major.
        if (dist <= 10 && dist >= 5)
        {
            GetComponent<Rigidbody>().AddForce((Player.transform.position - transform.position) * forceFactor);
            GetComponent<Rigidbody>().AddForce(transform.up * 2.5f);
        }
        else if (dist < 5)
        {
            GetComponent<Rigidbody>().AddForce((Player.transform.position - transform.position) * forceFactor * 2);
            GetComponent<Rigidbody>().AddForce(transform.up * 2.5f);
        }*/

        transform.LookAt(Player.transform);
        float dist = Vector3.Distance(Player.transform.position, transform.position);
        if (dist <= 10 && dist >= 5)
        {
            GetComponent<Rigidbody>().AddForce(transform.forward * forceFactor * 4);
            GetComponent<Rigidbody>().AddForce(transform.up * 1.5f);
        }
        else if (dist < 5 && dist >= 2)
        {
            GetComponent<Rigidbody>().AddForce(transform.forward * forceFactor * 8);
            GetComponent<Rigidbody>().AddForce(transform.up * 1.5f);
        }
        else if (dist < 2)
        {
            GetComponent<Rigidbody>().AddForce(transform.forward * forceFactor * 16);
            GetComponent<Rigidbody>().AddForce(transform.up * 1.5f);
        }
    }

    //Si toca amb el jugador, desapareix
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PlayerMove"))
        {
            Destroy(gameObject);
        }
    }

    private IEnumerator Efe()
    {
        //Despres de 5 segons, es destrueix.
        yield return new WaitForSeconds(20);
        Destroy(this.gameObject);
    }
}
