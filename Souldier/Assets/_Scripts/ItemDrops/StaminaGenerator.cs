﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaminaGenerator : MonoBehaviour
{
    public GameObject stamina;

    private void Start()
    {
        StartCoroutine(W8());
    }

    public IEnumerator W8()
    {
        yield return new WaitForSeconds(2);
        int j = 0;
        int n = Random.Range(1, 3);
        while (j != n)
        {
            float x, y, z;
            x = Random.Range(0, 0.5f);
            y = Random.Range(0, 0.5f);
            z = Random.Range(0, 0.5f);
            Instantiate(stamina, new Vector3(transform.position.x + x, transform.position.y + y, transform.position.z + z), Quaternion.identity);
            j++;
        }
        StartCoroutine(W8());
    }
}
