﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ObjectsCrack : MonoBehaviour
{
    //Game object en versió cracked
    public GameObject ObjectF, Caja;
    private GameObject res;

    private void Start()
    {
        res = GameObject.Find("Res");
    }

    private void OnCollisionEnter(Collision collision)
    {
        //si colisiona amb fireball, instancia la versio trencada i es destrueix.
        if (collision.gameObject.CompareTag("FireBall") || collision.gameObject.CompareTag("FireBall2") || collision.gameObject.CompareTag("FireBall3"))
        {
            Instantiate(ObjectF, transform.position, transform.rotation);
            Instantiate(Caja, res.transform.position, res.transform.rotation);
            Destroy(gameObject);
        }

        //si aquest té el tag projectile, es destruirà en contacte amb qualsevol cosa.
        if (collision.gameObject.CompareTag("Projectile"))
        {
            Instantiate(ObjectF, transform.position, transform.rotation);
            Instantiate(Caja, res.transform.position, res.transform.rotation);
            Destroy(gameObject);
        }

        //si li impacta un objecte amb tag projectile, també es destrueix.
        if (gameObject.CompareTag("Projectile"))
        {
            Instantiate(ObjectF, transform.position, transform.rotation);
            Instantiate(Caja, res.transform.position, res.transform.rotation);
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("BindedSword"))
        {
            Instantiate(ObjectF, transform.position, transform.rotation);
            Instantiate(Caja, res.transform.position, res.transform.rotation);
            Destroy(gameObject);
        }
    }
}
