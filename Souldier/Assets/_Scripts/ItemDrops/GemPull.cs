﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemPull : MonoBehaviour
{
    GameObject Player;
    public float forceFactor = 2;

    void Update()
    {
        //Busquem al jugador
        Player = GameObject.Find("Player");
        //Calculem la distancia entre jugador i item.
        float dist = Vector3.Distance(Player.transform.position, transform.position);

        //Si la distancia es troba entre 3 i 6, aplica una força constant al item cap al jugador.
        //Si es menor a 3 unitats, la força es major.
        if (dist <= 6 && dist >= 3)
        {
            GetComponent<Rigidbody>().AddForce((Player.transform.position - transform.position) * forceFactor);
        }
        else if (dist < 3)
        {
            GetComponent<Rigidbody>().AddForce((Player.transform.position - transform.position) * forceFactor * 3);
        }
    }
}
