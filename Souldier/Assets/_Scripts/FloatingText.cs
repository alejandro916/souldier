﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingText : MonoBehaviour
{
    public float DestroyTime = 3f;
    Vector3 Offset = new Vector3(0, 2, 0);
    Vector3 RandomIntensity = new Vector3(0.5f, 0, 0);
    void Start()
    {
        Destroy(gameObject, DestroyTime);
        transform.localPosition += Offset;
    }

    private void Update()
    {
        transform.forward = Camera.main.transform.forward;
    }
}
