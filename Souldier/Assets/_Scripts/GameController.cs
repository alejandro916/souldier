﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public void OnBoxCrack(GameObject obj)
    {
        Instantiate(obj, new Vector3(Random.Range(-20, -25), Random.Range(5, 7), Random.Range(20, 25)), Quaternion.identity);
    }
}
