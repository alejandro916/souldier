﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BossHud : MonoBehaviour
{

    public GameObject BossHUD;
    // Start is called before the first frame update
    void Start()
    {
        BossHUD = GameObject.Find("BossHUD");
        BossHUD.gameObject.SetActive(false);
        BossBehaviour.onDeath += Disable;// Subscripció al delegat de BossBehaviour
    }

    private void Disable()
    {
        BossHUD = GameObject.Find("BossHUD");
        BossHUD.gameObject.SetActive(false);
        Invoke("EndGame", 10);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PlayerMove"))// Al entrar al area de boss
        {
            // Fem display de la seva interficie
            BossHUD.gameObject.SetActive(true);
        }
    }

    void EndGame()
    {
        SceneManager.LoadScene("Grasias");
    }
}
