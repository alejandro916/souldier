﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class BossBehaviour : MonoBehaviour
{
    public delegate void BossDead();//Delegat
    public static event BossDead onDeath;//Event a la mort del boss

    public ParticleSystem particula1;
    public ParticleSystem particula2;
    private Animator animator;
    private bool animPlaying, running, mamadismo, alive;
    private GameObject player;
    private NavMeshAgent nma;
    public Image HpImage;
    public GameObject Llave;
    public GameObject CamTumba;
    public GameObject CamLlave;
    public GameObject CamPlayer;
    public GameObject tumba;
    private Animator tumbaAnimator;
    bool gira = false;

    // Start is called before the first frame update
    void Start()
    {
        tumbaAnimator = tumba.gameObject.GetComponent<Animator>();
        animator = gameObject.GetComponentInChildren<Animator>();
        nma = this.gameObject.GetComponent<NavMeshAgent>();
        CamPlayer = GameObject.Find("Main Camera");
        player = GameObject.Find("PlayerMove");
        animPlaying = false;
        running = false;
        mamadismo = false;
        alive = true;
        animator.Play("Idle");
    }

    // Update is called once per frame
    void Update()
    {
        if ((float) nma.stoppingDistance <= (float)nma.remainingDistance && running && alive)// Si el boss esta viu, i la distancia entre el seu objectiu es major a la distancia de parada de l'objectiu
        {
            running = false; //Permet correr
        }

        if (!animPlaying && ((float)nma.stoppingDistance) >= (float)nma.remainingDistance && alive)// Si no esta reproduint una animació, i la distancia entre el seu objectiu es menor a la distancia de parada de l'objectiu
        {
            Attack(); // Fa una animació d'atac
        }else
        {
            if (!running && alive) StartCoroutine(Correr()); // Si no, corre
        }
    }

    public void Attack()
    {
        StartCoroutine(Casteado());
    }

    IEnumerator Retroceder()
    {
        yield return new WaitForSeconds(4);
        animPlaying = true;
        yield return new WaitForSeconds(2);
        //TODO animacion retroceder
        animPlaying = false;
    }

    IEnumerator Correr()
    {
        if (!running)
        {
            if (!mamadismo)//abans de correr fa la animació "Mamadismo" un cop
            {
                animator.Play("Mamadismo");//A l'animator fa transició a la animacio "Corre"
                mamadismo = true;
                nma.isStopped = true;
            }
            running = true;
            yield return new WaitForSeconds(4);
            nma.isStopped = false;
            nma.speed = 15;
        }
    }

    IEnumerator Casteado()
    {
        nma.isStopped = true;
        if (gira && nma.isStopped)
        {
            transform.LookAt(player.transform);//Gira cap al jugador
            gira = false;
        }
        GetComponent<Rigidbody>().velocity = new Vector3 (0, 0, 0);//Para en sec
        nma.speed = 0;//No avança cap al jugador
        animator.Play("GolpeCasteado");
        animPlaying = true;
        yield return new WaitForSeconds(4.5f);
        nma.isStopped = false;
        animPlaying = false;
        mamadismo = false;
        gira = true;
    }

    IEnumerator Death(float time)
    {
        nma.isStopped = true;
        GameObject llave = null;
        if (GameObject.Find("Llave_Minotauro(Clone)") == null)
        {
            llave = Instantiate(Llave, new Vector3(180, 150, 242), Quaternion.identity);
        }
        
        animator.Play("LaCabesa");
        yield return new WaitForSeconds(time);
        this.gameObject.GetComponent<NavMeshAgent>().enabled = false;
        this.gameObject.GetComponent<BossController>().lookRadius = 1;
        this.gameObject.transform.position = new Vector3(424.2f, 18.24f, 464.7f);
        //Aqui empieza la cinematica donde se ve la llave caer y la tumba abrirse
        tumbaAnimator.SetBool("open", true);
        StartCoroutine(Secuencia());
        onDeath?.Invoke();//Event
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("FireBall"))
        {
            HpImage.fillAmount -= 0.01f;
            //HpImage.fillAmount = 0;
            if (HpImage.fillAmount <= 0)
            {
                onDeath?.Invoke();
                StartCoroutine(Death(1.5f));
            }
        }
        if (collision.gameObject.CompareTag("FireBall2"))
        {
            HpImage.fillAmount -= 0.02f;
            if (HpImage.fillAmount <= 0)
            {
                onDeath?.Invoke();
                StartCoroutine(Death(1.5f));
            }
        }
        if (collision.gameObject.CompareTag("FireBall3"))
        {
            HpImage.fillAmount -= 0.03f;
            if (HpImage.fillAmount <= 0)
            {
                onDeath?.Invoke();
                StartCoroutine(Death(1.5f));
            }
        }
    }

    private IEnumerator Secuencia()
    {
        CamPlayer.SetActive(false);
        CamTumba.SetActive(true);
        particula1.gameObject.SetActive(true);
        particula2.gameObject.SetActive(true);
        yield return new WaitForSeconds(4);
        CamLlave.SetActive(true);
        CamTumba.SetActive(false);
        particula1.gameObject.SetActive(false);
        particula2.gameObject.SetActive(false);
        yield return new WaitForSeconds(3);
        CamPlayer.SetActive(true);
        CamLlave.SetActive(false);
    }

}
