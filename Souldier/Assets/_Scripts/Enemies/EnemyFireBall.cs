﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFireBall : MonoBehaviour
{
    private Rigidbody rb;

    public ParticleSystem explosion;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        //Comença la corrutina de destrucció de la bola per evitar que s'acumulin si hi ha algun problema.
        StartCoroutine(Efe());
        //Apliquem una força a la bola per a que es llanci cap a endevant.
        rb.AddForce(transform.forward * 30, ForceMode.Impulse);
    }

    private void OnCollisionEnter(Collision collision)
    {
        //Si collisiona amb qualsevol cosa es destrueix i instancia particules d'explosio.
        ParticleSystem ps = Instantiate(explosion, transform.position, Quaternion.identity);
        Destroy(this.gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Shield"))
        {
            //Instanciar explosion
            ParticleSystem ps = Instantiate(explosion, transform.position, Quaternion.identity);
            Destroy(this.gameObject);
        }
    }

    private IEnumerator Efe()
    {
        //Despres de 5 segons, es destrueix.
        yield return new WaitForSeconds(5);
        Destroy(this.gameObject);
    }
}
