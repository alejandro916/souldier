﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BindedSkeleton : MonoBehaviour
{
    bool rdy = false;
    public Animator animator;

    bool walk = false;

    private void OnEnable()
    {
        EnemySkeleton.RdyToBind += Rdy;
    }

    private void OnDisable()
    {
        EnemySkeleton.RdyToBind -= Rdy;
    }

    private void Start()
    {
        animator.SetBool("verJugador", false);
        animator.SetBool("rangoJugador", false);
        animator.SetBool("rangoJugador2", false);
    }

    private void Update()
    {
        if (rdy)
        {
            if (Input.GetKey("w"))
            {
                animator.SetBool("verJugador", true);
                animator.SetBool("rangoJugador", false);
                walk = true;
            } else if (Input.GetKey("a"))
            {
                animator.SetBool("verJugador", true);
                animator.SetBool("rangoJugador", false);
                walk = true;
            } else if (Input.GetKey("s"))
            {
                animator.SetBool("verJugador", true);
                animator.SetBool("rangoJugador", false);
                walk = true;
            } else if (Input.GetKey("d"))
            {
                animator.SetBool("verJugador", true);
                animator.SetBool("rangoJugador", false);
                walk = true;
            } else if (Input.GetKeyUp("w") || Input.GetKeyUp("a") || Input.GetKeyUp("s") || Input.GetKeyUp("d"))
            {
                walk = false;
            }

            if (!walk)
            {
                animator.SetBool("verJugador", false);
            }

            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                animator.SetBool("verJugador", true);
                animator.SetBool("rangoJugador", true);
            }
            if (Input.GetKeyUp(KeyCode.Mouse0))
            {
                animator.SetBool("verJugador", true);
                animator.SetBool("rangoJugador", false);
            }
        }
    }

    public void Rdy()
    {
        rdy = true;
    }
}
