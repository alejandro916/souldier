﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class skeletonEnemyPath : MonoBehaviour
{
    public float lookRadius = 15f;
    Transform target; //El Jugador
    public Transform[] waypoints;
    private int nextWaypoint;
    Vector3 enemyPos;
    NavMeshAgent agent;
    public Animator animator;

    public float visionEnemy = 30f; //Distancia maxima de visió de l'enemic
    public float fov = 40f;  //Field Of View
    RaycastHit hit;

    void Start()
    {
        enemyPos = GetComponent<Transform>().position;
        target = PlayerManager.instance.player.transform;
        agent = GetComponent<NavMeshAgent>();
    }

    void Update()
    {

    }

    private void FixedUpdate()
    {
        if (target != null)
        {
            //Miramos la distancia con el Jugador
            float distance = Vector3.Distance(target.position, transform.position);

            animator.SetBool("verJugador", true);

            //Miramos si esta dentro del radio
            if (distance <= lookRadius)
            {
                agent.SetDestination(target.position);

                if (distance <= agent.stoppingDistance)
                {
                    FaceTarget();
                    animator.SetBool("rangoJugador", true);
                    StartCoroutine(W8());
                }
                else
                {
                    animator.SetBool("rangoJugador", false);
                    animator.SetBool("rangoJugador2", false);
                }
            }
            else if (Vector3.Distance(transform.position, target.transform.position) < visionEnemy)
            {
                //Si el jugador esta a distancia de ser visto por el enemigo

                var playerDir = (target.transform.position - transform.position).normalized;

                if (Vector3.Dot(transform.forward, playerDir) > 0 && Vector3.Angle(transform.forward, playerDir) < fov)
                {
                    //El Jugador esta delante del enemigo y dentro del campo de vision
                    //print("Player in front and in FOV of AI");

                    Debug.DrawLine(transform.position, transform.position + playerDir * visionEnemy, Color.red);
                    agent.SetDestination(target.position);
                }
            }
            else
            {
                if (CheckArrived())
                {
                    nextWaypoint = (nextWaypoint + 1) % waypoints.Length;
                    ActWaypoint();
                }
            }
        }
        


    }

    void FaceTarget()
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
    }

    public void ActDestiny(Vector3 DestinyPoint)
    {
        agent.destination = DestinyPoint;
        agent.Resume();
    }

    void ActWaypoint()
    {
        ActDestiny(waypoints[nextWaypoint].position);
    }

    public bool CheckArrived()
    {
        //Miramos que no quede distancia por recorrer y que no quede camino por recorrer
        return agent.remainingDistance <= agent.stoppingDistance && !agent.pathPending;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }

    private IEnumerator W8()
    {
        yield return new WaitForSeconds(3);
        animator.SetBool("rangoJugador2", true);
    }

}
