﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class EnemySkeleton : MonoBehaviour
{
    public Image HP;

    public GameObject[] listaPickups;
    public GameObject[] listaItems;
    public GameState gameState;
    public GameObject exp;
    public GameObject DmgTxt;

    private bool grounded = true;

    public ParticleSystem sb;
    ParticleSystem ps;
    bool soulbound = false;
    bool ready = false;
    bool bounded = false;
    GameObject player;
    BoxCollider hitbox;

    public GameObject dmgbox;

    //------------DELEGATS-----------
    public delegate void CanBound();
    public static event CanBound Binding;

    public delegate void Rdy();
    public static event Rdy RdyToBind;

    public delegate void IsDead();
    public static event IsDead Dead;

    public delegate void StopAtack();
    public static event StopAtack Stop;
    //--------------------------------

    private Transform playerpos;
    public GameObject head;

    public PlayerStats PS;

    private void OnEnable()
    {
        Abilities.Bounding += Ready;
    }

    private void OnDisable()
    {
        Abilities.Bounding -= Ready;
    }

    private void Start()
    {
        GetComponent<ThirdPersonCharacterControl>().enabled = false;
        player = GameObject.Find("PlayerMove");
        hitbox = gameObject.GetComponent<BoxCollider>();

        playerpos = GameObject.Find("FireBallSpawn").transform;
        GetComponent<BindedSkeleton>().enabled = false;
    }

    void Update()
    {
        Death();
        if (bounded)
        {
            gameObject.GetComponent<Rigidbody>().freezeRotation = true;
            transform.forward = player.transform.forward;
            //hitbox.center = new Vector3(0, 2.3f, 0);

            // funcio de salt
            if (Input.GetKeyDown("space") && grounded)
            {
                grounded = false;
                gameObject.GetComponent<Rigidbody>().AddForce(transform.up * 5, ForceMode.Impulse);
            }

            if (HP.fillAmount <= 0.001f)
            {
                RandomDrop();
                // Treiem aquest enemic d'una llista per poder controlar quants en queden
                gameState.ListaEnemigos.Remove(this.gameObject);
                Dead?.Invoke();
            }
        }
    }

    public void RandomDrop()
    {
        // agafa aleatoriament un item de la llista de drops que li posem i l'instancia
        // repeteix 2 cops.
        int i = 0;
        int r = Random.Range(0, 2);
        while (i != r)
        {
            float x, y, z;
            x = Random.Range(0, 0.5f);
            y = Random.Range(0, 0.5f);
            z = Random.Range(0, 0.5f);
            int rng = Random.Range(0, listaPickups.Length);
            Instantiate(listaPickups[rng], new Vector3(transform.position.x + x, transform.position.y + y, transform.position.z + z), Quaternion.identity);
            i++;
        }

        //Experiecia
        //Com a minim instanciarà 3 orbes de experiencia.
        int j = 0;
        int n = Random.Range(3, 6);
        while (j != n)
        {
            float x, y, z;
            x = Random.Range(0, 0.5f);
            y = Random.Range(0, 0.5f);
            z = Random.Range(0, 0.5f);
            Instantiate(exp, new Vector3(transform.position.x + x, transform.position.y + y, transform.position.z + z), Quaternion.identity);
            j++;
        }

        int g = Random.Range(0, 5);
        if (g == 1)
        {
            float x, y, z;
            x = Random.Range(0, 0.5f);
            y = Random.Range(0, 0.5f);
            z = Random.Range(0, 0.5f);

            int item = Random.Range(0, listaItems.Length);
            Instantiate(listaItems[item], new Vector3(transform.position.x + x, transform.position.y + y, transform.position.z + z), Quaternion.identity);
        }
    }

    //funcio de rebre danys
    public void Dmg(float dmg)
    {
        HP.fillAmount -= dmg;
        if (DmgTxt)
        {
            ShowDmgText(dmg);
        }
    }

    public void ShowDmgText(float dmg)
    {
        var go = Instantiate(DmgTxt, transform.position, Quaternion.identity, transform);
        go.GetComponent<TextMesh>().text = dmg * 100 + "";
    }

    #region SoulBound
    public void Death()
    {
        if (!bounded)
        {
            //Si la vida es 0 o menor...
            if (HP.fillAmount <= 0.001 && !soulbound)
            {
                int i = Random.Range(0, 3);

                if (gameObject.tag == "Skeleton100")
                {
                    i = 0;
                }

                // ... i si "i" es igual a 0...
                if (i == 0)
                {
                    soulbound = true;
                    ps = Instantiate(sb, new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z), Quaternion.identity);
                    ps.transform.parent = transform;
                    gameObject.tag = "CanBound";
                    StartCoroutine(Dying());
                }
                // ...es destrueix
                else
                {
                    Die();
                }
            }
        }
    }

    public void Ready()
    {
        if (ready)
        {
            bounded = true;
            StopAllCoroutines();
            if (ps)
            {
                Destroy(ps);
            }
            Binding?.Invoke();
            GetComponent<ThirdPersonCharacterControl>().enabled = true;
            
            head.transform.localScale = Vector3.zero;
            PS.GOHead = head;
            GetComponent<skeletonEnemyPath>().enabled = false;
            GetComponent<BindedSkeleton>().enabled = true;
            GetComponent<NavMeshAgent>().enabled = false;
            GetComponent<Rigidbody>().isKinematic = false;

            dmgbox.gameObject.tag = "BindedSword";

            RdyToBind?.Invoke();

            if (GetComponent<MagicAttack>())
            {
                Stop?.Invoke();
            }
            HP.fillAmount = 1;
        }
    }

    public void Die()
    {
        //... fa un Drop d'items random i es destrueix
        RandomDrop();
        // Treiem aquest enemic d'una llista per poder controlar quants en queden
        gameState.ListaEnemigos.Remove(this.gameObject);
        Destroy(this.gameObject);
    }

    private IEnumerator Dying()
    {
        yield return new WaitForSeconds(3);
        Die();
    }
    #endregion

    //Si entra en collisio amb la bola de foc, rep danys.
    private void OnCollisionEnter(Collision collision)
    {
        if (gameObject.tag == "Enemy" || gameObject.tag == "Skeleton100")
        {
            if (collision.gameObject.CompareTag("FireBall"))
            {
                Dmg(0.1f + PS.dmgmod);
            }
            if (collision.gameObject.CompareTag("FireBall2"))
            {
                Dmg(0.25f + PS.dmgmod);
            }
            if (collision.gameObject.CompareTag("FireBall3"))
            {
                Dmg(0.5f + PS.dmgmod);
            }
            if (collision.gameObject.CompareTag("Projectile"))
            {
                Dmg(0.25f);
            }
        }
        else
        {
            // si toca amb un enemic, rep mal.
            if (collision.gameObject.CompareTag("Enemy"))
            {
                Dmg(0.2f);
            }

            if (collision.gameObject.CompareTag("EnemyFireBall"))
            {
                Dmg(0.25f);
            }

            // en tocar amb el terra, podrà tornar a saltar.
            if (collision.gameObject.CompareTag("Floor"))
            {
                grounded = true;
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (soulbound && other.gameObject.CompareTag("PlayerMove"))
        {
            ready = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (gameObject.tag == "Enemy" || gameObject.tag == "Skeleton100")
        {
            if (soulbound && other.gameObject.CompareTag("PlayerMove"))
            {
                ready = true;
            }
            if (other.gameObject.CompareTag("BindedSword"))
            {
                Dmg(0.25f + PS.dmgmod);
            }
        }
        else
        {
            if (other.gameObject.CompareTag("dmg"))
            {
                Dmg(0.1f);
            }
            if (other.gameObject.CompareTag("dmgGolem"))
            {
                Dmg(0.2f);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (soulbound && other.gameObject.CompareTag("PlayerMove"))
        {
            ready = false;
        }
    }
}
