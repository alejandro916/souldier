﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class skeletonEnemy : MonoBehaviour
{

    public float lookRadius = 8f;
    Transform target; //El Jugador
    Vector3 enemyPos;
    NavMeshAgent agent;
    public Animator animator;

    public float visionEnemy = 15f; //Distancia maxima de vision del enemigo
    public float fov = 20f;  //Field Of View
    RaycastHit hit;


    void Start()
    {
        enemyPos = GetComponent<Transform>().position;
        target = PlayerManager.instance.player.transform;
        agent = GetComponent<NavMeshAgent>();
    }

    void Update()
    {

    }

    private void FixedUpdate()
    {
        //Miramos la distancia con el Jugador
        float distance = Vector3.Distance(target.position, transform.position);

        //Miramos si esta dentro del radio
        if (distance <= lookRadius)
        {
            agent.isStopped = false;
            animator.SetBool("verJugador", true);

            agent.SetDestination(target.position);

            if (distance <= agent.stoppingDistance)
            {
                FaceTarget();
                animator.SetBool("rangoJugador", true);
                StartCoroutine(W8());
            }
            else
            {
                animator.SetBool("rangoJugador", false);
                animator.SetBool("rangoJugador2", false);
            }
        }
        else
        {
            agent.SetDestination(enemyPos);
            if (agent.remainingDistance < 3)
            {
                agent.isStopped = true;
                animator.SetBool("verJugador", false);
            }
        }

        if (Vector3.Distance(agent.transform.position, target.transform.position) < visionEnemy)
        {

            //Si el jugador esta a distancia de ser visto por el enemigo
            var playerDir = (target.GetChild(0).transform.position - agent.transform.position).normalized;

            if (Vector3.Dot(agent.transform.forward, playerDir) > 0 && Vector3.Angle(agent.transform.forward, playerDir) < fov)
            {
                agent.isStopped = false;
                animator.SetBool("verJugador", true);

                //El Jugador esta delante del enemigo y dentro del campo de vision

                Debug.DrawLine(agent.transform.position, agent.transform.position + playerDir * visionEnemy, Color.red);
                agent.SetDestination(target.position);
            }
        }
        else
        {
            agent.SetDestination(enemyPos);
            if (agent.remainingDistance < 3)
            {
                agent.isStopped = true;
                animator.SetBool("verJugador", false);
            }
        }
    }

    void FaceTarget()
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }

    private IEnumerator W8()
    {
        yield return new WaitForSeconds(3);
        animator.SetBool("rangoJugador2", true);
    }
}