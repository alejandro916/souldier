﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyVision : MonoBehaviour
{
    public float radius;
    public Transform target;
    Vector3 v = Vector3.zero;
    float distance = 0f;
    float dot = 0f;
    float fov = 30f; // Field Of View
    float dotfov = 0f;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, radius);

        v = target.position - transform.position;
        distance = v.sqrMagnitude;

        v.Normalize();

        //Calculamos el producto punto de vision
        dotfov = Mathf.Cos(fov * 0.5f * Mathf.Deg2Rad);
        dot = Vector3.Dot(transform.forward, v);


        if ((distance <= radius * radius) && (dot >= dotfov))
        {
            Gizmos.color = Color.green;
        }
        else
        {
            Gizmos.color = Color.yellow;
        }

        Gizmos.DrawLine(transform.position, target.position);
    }
}
