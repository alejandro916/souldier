﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GolemChiquito : MonoBehaviour
{
    public Animator animator;
    private GameObject GolemBoss;
    private bool montadisimo = false;

    public float lookRadius = 8f;
    Transform target; //El Jugador
    Vector3 enemyPos;
    NavMeshAgent agent;

    public float visionEnemy = 15f; //Distancia maxima de vision del enemigo
    public float fov = 20f;  //Field Of View
    RaycastHit hit;

    void Start()
    {
        enemyPos = GetComponent<Transform>().position;
        target = PlayerManager.instance.player.transform;
        agent = GetComponent<NavMeshAgent>();
        GolemBoss = GameObject.Find("Golem");
    }

    void Update()
    {

    }

    private void FixedUpdate()
    {
        //Si el Golem esta vivo, el GolemChiquito hará sus funciones, una vez el Golem sea derrotado los GolemChiquitos se destruyen
        if (GolemBoss != null)
        {
            //Miramos la distancia con el Jugador
            float distance = Vector3.Distance(target.position, transform.position);

            //Miramos si esta dentro del radio
            if (distance <= lookRadius)
            {
                agent.isStopped = false;
                if (!montadisimo)
                {
                    agent.isStopped = true;
                    StartCoroutine(Montaje());
                }

                agent.SetDestination(target.position);

                if (distance <= agent.stoppingDistance)
                {
                    FaceTarget();
                    animator.SetBool("atk1", true);
                    StartCoroutine(SecondAtk());
                }
                else
                {
                    animator.SetBool("atk1", false);
                    animator.SetBool("atk2", false);
                }

                var playerDir = (target.GetChild(0).transform.position - agent.transform.position).normalized;

                if (Vector3.Dot(agent.transform.forward, playerDir) > 0 && Vector3.Angle(agent.transform.forward, playerDir) < fov)
                {
                    if (montadisimo)
                    {
                        agent.isStopped = false;
                        animator.SetBool("caminando", true);
                    }


                    //El Jugador esta delante del enemigo y dentro del campo de vision

                    Debug.DrawLine(agent.transform.position, agent.transform.position + playerDir * visionEnemy, Color.red);
                    agent.SetDestination(target.position);
                }

            }
            else
            {
                agent.SetDestination(enemyPos);
                if (agent.remainingDistance < 6.5)
                {
                    agent.isStopped = true;
                    animator.SetBool("caminando", false);
                }
            }
        }
        else
        {
            Destroy(this.gameObject);
        }
       
    }

    void FaceTarget()
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }

    private IEnumerator Montaje()
    {
        //yield return new WaitForSeconds(2);
        animator.SetBool("montado", true);
        yield return new WaitForSeconds(6);
        montadisimo = true;
        agent.isStopped = false;
        animator.SetBool("caminando", true);
    }

    private IEnumerator SecondAtk()
    {
        yield return new WaitForSeconds(10);
        animator.SetBool("atk2", true);
    }

    /*
    private IEnumerator W8()
    {
        float n = Random.Range(1, 3);
        yield return new WaitForSeconds(n);
            StartCoroutine(Attack());
    }*/
}