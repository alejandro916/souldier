﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GolemHUD : MonoBehaviour
{

    public GameObject GolemHud;
    public Animator Golem;
    // Start is called before the first frame update
    void Start()
    {
        GolemHud.gameObject.SetActive(false);
        GolemController.onDeath += Disable;// Subscripció al delegat de BossBehaviour
    }

    private void Disable()
    {
        
        GolemHud.gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PlayerMove"))// Al entrar al area de boss
        {
            // Fem display de la seva interficie
            
            GolemHud.gameObject.SetActive(true);
        }
    }
}
