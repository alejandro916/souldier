﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GolemDrop : MonoBehaviour
{
    public Image HP;

    public GameObject[] listaPickups;
    public GameObject[] listaItems;
    public GameState gameState;
    public GameObject exp;
    public GameObject DmgTxt;

    public PlayerStats PS;

    void Update()
    {
        //Si la vida es 0 o menor...
        if (HP.fillAmount <= 0.001)
        {
            Die();
        }
    }

    public void RandomDrop()
    {
        // agafa aleatoriament un item de la llista de drops que li posem i l'instancia
        // repeteix 2 cops.
        int i = 0;
        int r = Random.Range(0, 2);
        while (i != r)
        {
            float x, y, z;
            x = Random.Range(0, 0.5f);
            y = Random.Range(0, 0.5f);
            z = Random.Range(0, 0.5f);
            int rng = Random.Range(0, listaPickups.Length);
            Instantiate(listaPickups[rng], new Vector3(transform.position.x + x, transform.position.y + y, transform.position.z + z), Quaternion.identity);
            i++;
        }

        //Experiecia
        //Com a minim instanciarà 3 orbes de experiencia.
        int j = 0;
        int n = Random.Range(3, 6);
        while (j != n)
        {
            float x, y, z;
            x = Random.Range(0, 0.5f);
            y = Random.Range(0, 0.5f);
            z = Random.Range(0, 0.5f);
            Instantiate(exp, new Vector3(transform.position.x + x, transform.position.y + y, transform.position.z + z), Quaternion.identity);
            j++;
        }
        
        int g = Random.Range(0, 2);
        if (g == 1)
        {
            float x, y, z;
            x = Random.Range(0, 0.5f);
            y = Random.Range(0, 0.5f);
            z = Random.Range(0, 0.5f);

            int item = Random.Range(0, listaItems.Length);
            Instantiate(listaItems[item], new Vector3(transform.position.x + x, transform.position.y + y, transform.position.z + z), Quaternion.identity);
        }
    }

    //funcio de rebre danys
    public void Dmg(float dmg)
    {
        HP.fillAmount -= dmg;
        if (DmgTxt)
        {
            ShowDmgText(dmg);
        }
    }

    public void ShowDmgText(float dmg)
    {
        var go = Instantiate(DmgTxt, transform.position, Quaternion.identity, transform);
        go.GetComponent<TextMesh>().text = dmg * 100 + "";
    }

    public void Die()
    {
        //... fa un Drop d'items random i es destrueix
        RandomDrop();
        gameState.ListaEnemigos.Remove(this.gameObject);
        Destroy(this.gameObject);
    }
    
    //Si entra en collisio amb la bola de foc, rep 50% de danys.
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("FireBall"))
        {
            Dmg(0.1f + PS.dmgmod);
        }
        if (collision.gameObject.CompareTag("FireBall2"))
        {
            Dmg(0.25f + PS.dmgmod);
        }
        if (collision.gameObject.CompareTag("FireBall3"))
        {
            Dmg(0.5f + PS.dmgmod);
        }
        if (collision.gameObject.CompareTag("Projectile"))
        {
            Dmg(0.15f);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("BindedSword"))
        {
            Dmg(0.25f + PS.dmgmod);
        }
    }
}
