﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class GolemController : MonoBehaviour
{
    public delegate void GolemDead();//Delegat
    public static event GolemDead onDeath;//Event a la mort del golem

    private Animator animator;
    private bool alive = true;
    private bool montadisimo = false;
    public Image HpImage;

    public float lookRadius = 8f;
    Transform target; //El Jugador
    Vector3 enemyPos;
    NavMeshAgent agent;

    public float visionEnemy = 15f; //Distancia maxima de vision del enemigo
    public float fov = 20f;  //Field Of View
    RaycastHit hit;

    void Start()
    {
        enemyPos = GetComponent<Transform>().position;
        target = PlayerManager.instance.player.transform;
        agent = GetComponent<NavMeshAgent>();
        animator = gameObject.GetComponent<Animator>();
    }

    void Update()
    {

    }

    private void FixedUpdate()
    {
        if (alive)
        {
            //Miramos la distancia con el Jugador
            float distance = Vector3.Distance(target.position, transform.position);
            //Miramos si esta dentro del radio
            if (distance <= lookRadius)
            {
                agent.isStopped = false;
                if (!montadisimo)
                {
                    agent.isStopped = true;
                    StartCoroutine(Montaje());
                }

                agent.SetDestination(target.position);

                if (distance <= agent.stoppingDistance)
                {
                    FaceTarget();
                    animator.SetBool("atk1", true);
                    StartCoroutine(SecondAtk());
                }
                else
                {
                    animator.SetBool("atk1", false);
                    animator.SetBool("atk2", false);
                }

                var playerDir = (target.GetChild(0).transform.position - agent.transform.position).normalized;

                if (Vector3.Dot(agent.transform.forward, playerDir) > 0 && Vector3.Angle(agent.transform.forward, playerDir) < fov)
                {
                    if (montadisimo)
                    {
                        agent.isStopped = false;
                        animator.SetBool("caminando", true);
                    }
                    //El Jugador esta delante del enemigo y dentro del campo de vision

                    Debug.DrawLine(agent.transform.position, agent.transform.position + playerDir * visionEnemy, Color.red);
                    agent.SetDestination(target.position);
                }

            }
            else
            {
                agent.SetDestination(enemyPos);
                if (agent.remainingDistance < 8.5)
                {
                    agent.isStopped = true;
                    animator.SetBool("caminando", false);
                }
            }
        }   
    }

    void FaceTarget()
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }

    IEnumerator Death()
    {
        alive = false;
        Destroy(this.gameObject);
        yield return new WaitForSeconds(1);
        onDeath?.Invoke();//Event
    }

    private IEnumerator Montaje()
    {
        //yield return new WaitForSeconds(2);
        animator.SetBool("montado", true);
        yield return new WaitForSeconds(8);
        montadisimo = true;
        agent.isStopped = false;
        animator.SetBool("caminando", true);
    }

    private IEnumerator SecondAtk()
    {
        yield return new WaitForSeconds(10);
        animator.SetBool("atk2", true);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("FireBall"))
        {
            HpImage.fillAmount -= 0.01f;
            if (HpImage.fillAmount <= 0.02)
            {
                onDeath?.Invoke();
                StartCoroutine(Death());
            }
        }

        if (collision.gameObject.CompareTag("FireBall2"))
        {
            HpImage.fillAmount -= 0.02f;
            if (HpImage.fillAmount <= 0.02)
            {
                onDeath?.Invoke();
                StartCoroutine(Death());
            }
        }

        if (collision.gameObject.CompareTag("FireBall3"))
        {
            HpImage.fillAmount -= 0.03f;
            if (HpImage.fillAmount <= 0.02)
            {
                onDeath?.Invoke();
                StartCoroutine(Death());
            }
        }
    }
}