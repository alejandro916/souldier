﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    public GameObject EnemyFireBall;

    public float lookRadius = 8f;
    Transform target; //El Jugador
    Vector3 enemyPos;
    NavMeshAgent agent;

    public float visionEnemy = 15f; //Distancia maxima de vision del enemigo
    public float fov = 20f;  //Field Of View
    RaycastHit hit;

    bool atacking = false;
    bool firstatack = false;

    void Start()
    {
        enemyPos = GetComponent<Transform>().position;
        target = PlayerManager.instance.player.transform;
        agent = GetComponent<NavMeshAgent>();
    }

    void Update()
    {

    }

    private void FixedUpdate()
    {
        //Miramos la distancia con el Jugador
        float distance = Vector3.Distance(target.position, transform.position);

        //Miramos si esta dentro del radio
        if (distance <= lookRadius)
        {
            //----------------------------------------
            if (firstatack)
            {
                firstatack = false;
                atacking = true;
                StartCoroutine(Attack());
            }
            //----------------------------------------

            agent.SetDestination(target.position);

            if (distance <= agent.stoppingDistance)
            {
                FaceTarget();
            }
        }
        else if (Vector3.Distance(agent.transform.position, target.transform.position) < visionEnemy)
        {
            //Si el jugador esta a distancia de ser visto por el enemigo
            var playerDir = (target.GetChild(0).transform.position - agent.transform.position).normalized;
            
            if (Vector3.Dot(agent.transform.forward, playerDir) > 0 && Vector3.Angle(agent.transform.forward, playerDir) < fov)
            {
                //El Jugador esta delante del enemigo y dentro del campo de vision
                print("Player in front and in FOV of AI");

                //----------------------------------------
                if (firstatack)
                {
                    firstatack = false;
                    atacking = true;
                    StartCoroutine(Attack());
                }
                //----------------------------------------

                Debug.DrawLine(agent.transform.position, agent.transform.position + playerDir * visionEnemy, Color.red);
                agent.SetDestination(target.position);

                /*if (Physics.Raycast(agent.transform.position, agent.transform.position + playerDir, out hit, visionEnemy))
                {
                    if (hit.collider.tag == "Player")
                    {
                        print("THE RAY IS HITTING THE PLAYER");
                        //Destroy(this);
                    }

                    if (hit.collider.tag == "Wall")
                    {
                        print("THE RAY IS HITTING THE WALL");
                        //Destroy(this);
                    }

                }*/
            }
        }
        else
        {
            agent.SetDestination(enemyPos);
            firstatack = true;
            atacking = false;
        }
    }

    void FaceTarget()
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }

    public IEnumerator Attack()
    {
        //enemic instancia una bola de foc que fa mal al jugador si el toca.
        Instantiate(EnemyFireBall, new Vector3(transform.position.x, transform.position.y + 3, transform.position.z), transform.rotation);
        //espera entre 2 y 4 segons avans de tornar a atacar.
        float n = Random.Range(2, 4);
        yield return new WaitForSeconds(n);

        //torna a començar.
        if (atacking)
        {
            StartCoroutine(Attack());
        }
    }
    /*
    private IEnumerator W8()
    {
        float n = Random.Range(1, 3);
        yield return new WaitForSeconds(n);
            StartCoroutine(Attack());
    }*/
}