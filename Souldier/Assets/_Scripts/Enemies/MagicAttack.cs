﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicAttack : MonoBehaviour
{
    public GameObject EnemyFireBall;
    public bool activated = false;

    private void OnEnable()
    {
        EnemyDrop.Stop += StopShooting;
    }

    private void OnDisable()
    {
        EnemyDrop.Stop -= StopShooting;
    }

    //aquest script només serverix per a tests
    private void Start()
    {
        //petita corrutina per esperarse avans de començar a atacar
        StartCoroutine(W8());
    }

    public IEnumerator Attack()
    {
        //enemic instancia una bola de foc que fa mal al jugador si el toca.
        Instantiate(EnemyFireBall, new Vector3(transform.position.x, transform.position.y + 3, transform.position.z), transform.rotation);
        //espera entre 2 y 4 segons avans de tornar a atacar.
        float n = Random.Range(2, 4);
        yield return new WaitForSeconds(n);
        //torna a començar.
        StartCoroutine(Attack());
    }

    private IEnumerator W8()
    {
        float n = Random.Range(1, 3);
        yield return new WaitForSeconds(n);
        StartCoroutine(Attack());
    }

    public void StopShooting()
    {
        StopAllCoroutines();
    }
}
