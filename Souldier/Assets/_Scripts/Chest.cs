﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : MonoBehaviour
{
    public List<GameObject> Loot;
    bool open = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DropLoot()
    {
        if (!open)
        {
            open = true;
            this.GetComponent<Animator>().Play("open");
            Instantiate(Loot[Random.Range(0, Loot.Count)], new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y + 2.5f, this.gameObject.transform.position.z), Quaternion.identity);
        }
    }
}
