﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HideHUD : MonoBehaviour
{
    private string escena;
    private bool played = false;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        escena = SceneManager.GetActiveScene().name;
        if (!played)
        {
            if (escena == "Cementerio")
            {
                StartCoroutine(hideHUD());
                played = true;
            }
        }
    }

    private IEnumerator hideHUD()
    {
        this.gameObject.GetComponentInChildren<Canvas>().enabled = false;
        yield return new WaitForSeconds(13);
        Debug.Log("hello hud?");
        this.gameObject.GetComponentInChildren<Canvas>().enabled = true;
    }
}
