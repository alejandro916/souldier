﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Settings : MonoBehaviour
{
    public Slider[] volumenSlider;
    public List<AudioSource> Audio = new List<AudioSource>();
    // 0 serà musica ambiental
    // 1 audio de projectils
    // 2 audio d'efectes

    public Ajustes Ajustes;
    public GameObject pause, settings;

    // Start is called before the first frame update
    void Start()
    {
        if (Audio[0] == null)
        {
            Audio[0] = GameObject.Find("Musica").GetComponent<AudioSource>();
        }
        if (Audio[1] == null)
        {
            Audio[1] = GameObject.Find("ProjectileFX").GetComponent<AudioSource>();
        }
        if (Audio[2] == null)
        {
            Audio[2] = GameObject.Find("AudioAmbiental").GetComponent<AudioSource>();
        }
        if (Audio.Count > 0)
        {
            volumenSlider[0].value = Ajustes.MusicVolume;
            Audio[0].GetComponent<AudioSource>().volume = volumenSlider[0].value;
        }
        if (Audio.Count > 1)
        {
            volumenSlider[1].value = Ajustes.ProjectileVolume;
            Audio[1].GetComponent<AudioSource>().volume = volumenSlider[1].value;
        }
        if (Audio.Count > 2)
        {
            volumenSlider[2].value = Ajustes.AmbientalVolume;
            Audio[2].GetComponent<AudioSource>().volume = volumenSlider[2].value; ;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Audio[0] == null)
        {
            Audio[0] = GameObject.Find("Musica").GetComponent<AudioSource>();
        }
        if (Audio[1] == null)
        {
            Audio[1] = GameObject.Find("ProjectileFX").GetComponent<AudioSource>();
        }
        if (Audio[2] == null)
        {
            Audio[2] = GameObject.Find("AudioAmbiental").GetComponent<AudioSource>();
        }
        if (Audio.Count > 0) {
            Audio[0].GetComponent<AudioSource>().volume = volumenSlider[0].value;//Valor del volum de la musica
            Ajustes.MusicVolume = volumenSlider[0].value;
        }
        if (Audio.Count > 1)
        {
            Audio[1].GetComponent<AudioSource>().volume = volumenSlider[1].value;//Valor del volum del projectil
            Ajustes.ProjectileVolume = volumenSlider[1].value;
        }
        if (Audio.Count > 2)
        {
            Audio[2].GetComponent<AudioSource>().volume = volumenSlider[2].value;//Valor del volum dels efectes ambientals
            Ajustes.AmbientalVolume = volumenSlider[2].value;
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            settings.gameObject.SetActive(false);
        }
    }

    public void ReturnToPause()
    {
        settings.gameObject.SetActive(false);
        pause.gameObject.SetActive(true);
    }
}
