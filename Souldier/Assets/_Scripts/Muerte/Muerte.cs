﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Muerte : MonoBehaviour
{
    public GameObject CanvasMuerte, HUD;
    
    void Start()
    {
        CanvasMuerte.SetActive(false);
    }

    private void OnEnable()
    {
        Player.Die += Dead;
    }

    private void OnDisable()
    {
        Player.Die -= Dead;
    }

    private void Dead()
    {
        HUD.SetActive(false);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        CanvasMuerte.SetActive(true);
    }

    public void Continue()
    {
        Destroy(GameObject.Find("PlayerMove"));
        Destroy(GameObject.Find("Canvas"));
        Destroy(GameObject.Find("CameraMinimap"));
        if (SceneManager.GetActiveScene().name.Equals("Cementerio"))
        {
            SceneManager.LoadScene("Forest 1");
        } else
        {
            SceneManager.LoadScene("Forest");
        }
        CanvasMuerte.SetActive(false);
    }

    public void Surrender()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
